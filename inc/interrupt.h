#include "configure.h"

#define INTERRUPT_VBLANK 1
#define INTERRUPT_LCD_STAT 2
#define INTERRUPT_TIMER 4
#define INTERRUPT_SERIAL 8
#define INTERRUPT_JOYPAD 16

typedef struct interrupt {
  bool master;
  bool enable;
  uint8_t reg;
  uint8_t flags;
}interrupt;

extern interrupt cpu_interrupt;

void interrupt_init(void);
