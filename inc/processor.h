#include "configure.h"
typedef struct reg {
  union {
    struct {
      uint8_t l; /* Lower byte */
      uint8_t h; /* Higher byte */
    };
    uint16_t word;
  };
}reg;


/*
 * Zero Flag (Z):
 * Subtract Flag (N):
 * Half Carry Flag (H):
 * Carry Flag (C):
 * Result Stored (F):
 */

typedef struct general_regs{
  reg AF;
  reg BC;
  reg DE;
  reg HL;
  reg SP; /* Stack Pointer */
  reg PC; /* Program Counter */
  int flag_z; /* Zero */
  int flag_n; /* Subtract */
  int flag_h; /* Half Carry */
  int flag_c; /* Carry */ 
}general_regs;


typedef struct cpu{
  general_regs regs;
  uint16_t fe;
  uint8_t opcode;
  bool halt;
  uint64_t cycle; /* Machine Cycles */
}cpu;

extern cpu g_cpu;

void execute_next(void);

void execute_extended_next(void);

uint16_t stack_pop(void);

void stack_push(uint16_t value);


/******************* ALU FLAGS HANDLERS ******************/
void flags_inc_dec(uint8_t prev_value, uint8_t curr_value, int state);
void flags_add(uint8_t result, uint8_t prev, uint8_t value);
void flags_sub(uint8_t result, uint8_t prev, uint8_t value);
void flags_and(void);
void flags_or(void);
void flags_xor(void);

/*********************************************************/

/******************** RR FLAGS HANDLERS ******************/
void flags_rotate(uint8_t value);
/*********************************************************/
void flags_swap(uint8_t value);

/************************* Core *************************/
int processor_init(void);
int cpu_step(void);
/********************************************************/ 


