#include "configure.h"


#define MBC_NONE 0
#define MBC_MBC1 1
#define MBC_MBC2 2
#define MBC_MBC3 3
#define MBC_MBC5 5
#define MBC_RUMBLE 15
#define MBC_HUC1 0xC1
#define MBC_HUC3 0xC3



/* Memory Bank Controller */
typedef struct mbc{
  uint8_t type;
  uint8_t model;
  uint8_t romsize;
  uint8_t ramsize;
  bool ram_bank_enable;
  bool mode;
}mbc; 

typedef struct rom{
  uint8_t *bytes; /* The actual game */
  uint8_t *game_bank_rom;/* Read only */
  uint8_t bank; /* Current bank */
  char title[16]; /* Name of Game */
  mbc contr; /* Info of Cartridge */
}rom;

typedef struct ram{
  uint8_t w_ram[0x2000]; /* work ram */
  uint8_t h_ram[0x80]; /* high ram */
  int mode; /* Mode select */
  bool enable; /* External RAM */
}ram;


extern rom g_rom;
extern ram g_ram;

uint8_t read_address(uint16_t address);

void write_address(uint16_t address, uint8_t data);
