#include "configure.h"


typedef struct emu{
  bool pause;
  bool run;
  bool kill;
  uint64_t ticks;
}emu_info;

extern emu_info emu_data;

int run(char *cart);
