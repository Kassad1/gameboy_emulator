#include "configure.h"

#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768

int ui_init(void);
void ui_handle_events(void);
