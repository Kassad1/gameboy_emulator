#include "processor.h"
#include "mem.h"

static void rlc_8bit_reg(uint8_t *reg)
{
  bool bit_set = (*reg) & 0x80;
  *reg <<= 1;
  
  if(bit_set) {
    g_cpu.regs.flag_c = 1;
    *reg = (*reg) | 0x01;
  }
  
  flags_rotate(*reg);
}

static void rlc_8bit_address(uint16_t address)
{
  uint8_t reg = read_address(address);
  bool bit_set = reg & 0x80;
  reg <<= 1;
  
  if(bit_set) {
    g_cpu.regs.flag_c = 1;
    reg |= 0x01;
  }

  flags_rotate(reg);
  write_address(address, reg);
}


static void rrc_8bit_reg(uint8_t *reg)
{
  /* Moving the bit to RSB */
  bool bit_set = (*reg) & 0x01;
  *reg >>= 1;
  
  if(bit_set) {
    g_cpu.regs.flag_c = 1;
    *reg = (*reg) | 0x80;
  }
  
  flags_rotate(*reg);
}

static void rrc_8bit_address(uint16_t address)
{
  /* Moving the bit to RSB */
  uint8_t reg = read_address(address);
  bool bit_set = reg & 0x80;

  reg >>= 1;
  
  if(bit_set) {
    g_cpu.regs.flag_c = 1;
    reg |= 0x01;
  }
  
  flags_rotate(reg);
  write_address(address, reg);
}

static void rl_8bit_reg(uint8_t *reg)
{
  bool bit_set = *reg & 0x80;
  bool carry = g_cpu.regs.flag_c;

  *reg <<= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  if(carry)
    *reg |= 0x01;

  flags_rotate(*reg);
}

static void rl_8bit_address(uint16_t address)
{
  uint8_t reg = read_address(address);
  bool bit_set = reg & 0x80;
  bool carry = g_cpu.regs.flag_c;

  reg <<= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  if(carry)
    reg |= 0x01;

  flags_rotate(reg);
  write_address(address, reg);
}

/* Double check */
static void rr_8bit_reg(uint8_t *reg)
{
  bool bit_set = *reg & 0x10;
  bool carry = g_cpu.regs.flag_c;

  *reg >>= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  if(carry)
    *reg |= 0x80;

  flags_rotate(*reg);
}

static void rr_8bit_address(uint16_t address)
{
  uint8_t reg = read_address(address);
  bool bit_set = reg & 0x10;
  bool carry = g_cpu.regs.flag_c;

  reg >>= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;
  if(carry)
    reg |= 0x08;

  flags_rotate(reg);
  write_address(address, reg);
}

static void sla_8bit_reg(uint8_t *reg)
{
  bool bit_set = *reg & 0x80;

  *reg <<= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  flags_rotate(*reg);
}

static void sla_8bit_address(uint16_t address)
{
  uint8_t reg = read_address(address);
  bool bit_set = reg & 0x80;

  reg <<= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  flags_rotate(reg);
  write_address(address, reg);
}

static void sra_8bit_reg(uint8_t *reg)
{
  bool bit_set = *reg & 0x01;
  bool msb = *reg & 0x80;

  *reg >>= 1;
  
  if(bit_set)
    g_cpu.regs.flag_c = 1;
  
  if(msb)
    *reg = (*reg) & 0x80;
     
  flags_rotate(*reg);
}

static void sra_8bit_address(uint16_t address)
{
  uint8_t reg = read_address(address);
  bool bit_set = reg & 0x01;
  bool msb = reg & 0x80;

  reg >>= 1;
  
  if(bit_set)
    g_cpu.regs.flag_c = 1;
  
  if(msb)
    reg = (reg) & 0x80;
     
  flags_rotate(reg);
  write_address(address, reg);
}

static void swap_8bit_reg(uint8_t *reg)
{
  *reg = ((*reg & 0xF0) >> 4) | ((*reg & 0x0F) << 4);
  
  flags_swap(*reg);
  
}

static void swap_8bit_address(uint16_t address)
{
  uint8_t reg = read_address(address);
  reg = ((reg & 0xF0) >> 4) | ((reg & 0x0F) << 4);

  flags_swap(reg);
  write_address(address, reg);
}

static void srl_8bit_reg(uint8_t *reg)
{
  bool bit_set = *reg & 0x01;
  *reg >>= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  flags_rotate(*reg);
}

static void srl_8bit_address(uint16_t address)
{
  uint8_t reg = read_address(address);
  bool bit_set = reg & 0x01;
  reg >>= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  flags_rotate(reg);

  write_address(address, reg);
}

static void bit_check_8bit_reg(uint8_t reg, uint8_t bit_check)
{
  g_cpu.regs.flag_z = ((reg & bit_check) == 0) ? 0 : 1;
  g_cpu.regs.flag_n = 0;
  g_cpu.regs.flag_h = 1; 
}


static void bit_check_8bit_address(uint8_t address, uint8_t bit_check)
{
  uint8_t value = read_address(address);
  g_cpu.regs.flag_z = ((value & bit_check) == 0) ? 0 : 1;
  g_cpu.regs.flag_n = 0;
  g_cpu.regs.flag_h = 1;
}

static void res_check_8bit_reg(uint8_t *reg, uint8_t bit_reset)
{
  *reg = *reg & (~bit_reset);
}

static void res_check_8bit_address(uint16_t address, uint8_t bit_reset)
{
  uint16_t value = (read_address(address) & (~bit_reset));

  write_address(address, value);
}

static void set_check_8bit_reg(uint8_t *reg, uint8_t bit_set){
  *reg = *reg | bit_set;

  g_cpu.cycle += 2;
}

static void set_check_8bit_address(uint16_t address, uint8_t bit_set){
  uint8_t value = read_address(address) | bit_set;
  write_address(address, value);

  g_cpu.cycle += 4;
}

void execute_extended_next(void){
  uint8_t opcode = read_address(g_cpu.regs.PC.word++);
  switch(opcode){
    /******** 8-bit RSB ********/
    /** Rotate n left. Old bit 7 to Carry Flag **/
    /* RLC B */
    case 0x00: rlc_8bit_reg(&g_cpu.regs.BC.h); break;
    /* RLC C */
    case 0x01: rlc_8bit_reg(&g_cpu.regs.BC.l); break;
    /* RLC D */
    case 0x02: rlc_8bit_reg(&g_cpu.regs.DE.h); break;
    /* RLC E */
    case 0x03: rlc_8bit_reg(&g_cpu.regs.DE.l); break;
    /* RLC H */
    case 0x04: rlc_8bit_reg(&g_cpu.regs.HL.h); break;
    /* RLC L */
    case 0x05: rlc_8bit_reg(&g_cpu.regs.HL.l); break;
    /* RLC (HL) */
    case 0x06: rlc_8bit_address(g_cpu.regs.HL.word); break;
    /* RLC A */
    case 0x07: rlc_8bit_reg(&g_cpu.regs.AF.h); break;


    /** Rotate n right. Old bit 7 to Carry Flag **/
    /* RRC B */
    case 0x08: rrc_8bit_reg(&g_cpu.regs.BC.h); break;
    /* RRC C */
    case 0x09: rrc_8bit_reg(&g_cpu.regs.BC.l); break;
    /* RRC D */
    case 0x0A: rrc_8bit_reg(&g_cpu.regs.DE.h); break;
    /* RRC E */
    case 0x0B: rrc_8bit_reg(&g_cpu.regs.DE.l); break;
    /* RRC H */
    case 0x0C: rrc_8bit_reg(&g_cpu.regs.HL.h); break;
    /* RRC L */
    case 0x0D: rrc_8bit_reg(&g_cpu.regs.HL.l); break;
    /* RRC (HL) */
    case 0x0E: rrc_8bit_address(g_cpu.regs.HL.word); break;
    /* RRC A */
    case 0x0F: rrc_8bit_reg(&g_cpu.regs.AF.h); break;


    /** Rotate n left through Carry flag **/
    /* RL B */
    case 0x10: rl_8bit_reg(&g_cpu.regs.BC.h); break;
    /* RL C */
    case 0x11: rl_8bit_reg(&g_cpu.regs.BC.l); break;
    /* RL D */
    case 0x12: rl_8bit_reg(&g_cpu.regs.DE.h); break;
    /* RL E */
    case 0x13: rl_8bit_reg(&g_cpu.regs.DE.l); break;
    /* RL H */
    case 0x14: rl_8bit_reg(&g_cpu.regs.HL.h); break;
    /* RL L */
    case 0x15: rl_8bit_reg(&g_cpu.regs.HL.l); break;
    /* RL (HL) */
    case 0x16: rl_8bit_address(g_cpu.regs.HL.word); break;
    /* RL A */
    case 0x17: rl_8bit_reg(&g_cpu.regs.AF.h); break;


    /** Rotate n right through Carry Flag **/
    /* RR B */
    case 0x18: rr_8bit_reg(&g_cpu.regs.BC.h); break;
    /* RR C */
    case 0x19: rr_8bit_reg(&g_cpu.regs.BC.l); break;
    /* RR D */
    case 0x1A: rr_8bit_reg(&g_cpu.regs.DE.h); break;
    /* RR E */
    case 0x1B: rr_8bit_reg(&g_cpu.regs.DE.l); break;
    /* RR H */
    case 0x1C: rr_8bit_reg(&g_cpu.regs.HL.h); break;
    /* RR L */
    case 0x1D: rr_8bit_reg(&g_cpu.regs.HL.l); break;
    /* RR (HL) */
    case 0x1E: rr_8bit_address(g_cpu.regs.HL.word); break;
    /* RR A */
    case 0x1F: rr_8bit_reg(&g_cpu.regs.AF.h); break;


    /** Shift n left into Carry. MSB doesn't change **/
    /* SLA B */
    case 0x20: sla_8bit_reg(&g_cpu.regs.BC.h); break;
    /* SLA C */
    case 0x21: sla_8bit_reg(&g_cpu.regs.BC.l); break;
    /* SLA D */
    case 0x22: sla_8bit_reg(&g_cpu.regs.DE.h); break;
    /* SLA E */
    case 0x23: sla_8bit_reg(&g_cpu.regs.DE.l); break;
    /* SLA H */
    case 0x24: sla_8bit_reg(&g_cpu.regs.HL.h); break;
    /* SLA L */
    case 0x25: sla_8bit_reg(&g_cpu.regs.HL.l); break;
    /* SLA (HL) */
    case 0x26: sla_8bit_address(g_cpu.regs.HL.word); break;
    /* SLA A */
    case 0x27: sla_8bit_reg(&g_cpu.regs.AF.h); break;


    /** Shift n right into Carry. MSB doesn't change **/
    /* SRA B */
    case 0x28: sra_8bit_reg(&g_cpu.regs.BC.h); break;
    /* SRA C */
    case 0x29: sra_8bit_reg(&g_cpu.regs.BC.l); break;
    /* SRA D */
    case 0x2A: sra_8bit_reg(&g_cpu.regs.DE.h); break;
    /* SRA E */
    case 0x2B: sra_8bit_reg(&g_cpu.regs.DE.l); break;
    /* SRA H */
    case 0x2C: sra_8bit_reg(&g_cpu.regs.HL.h); break;
    /* SRA L */
    case 0x2D: sra_8bit_reg(&g_cpu.regs.HL.l); break;
    /* SRA (HL) */
    case 0x2E: sra_8bit_address(g_cpu.regs.HL.word); break;
    /* SRA A */
    case 0x2F: sra_8bit_reg(&g_cpu.regs.AF.h); break;


    /** Swap upper 4 bits in register r8 and the lower ones **/
    /* SWAP B */
    case 0x30: swap_8bit_reg(&g_cpu.regs.BC.h); break;
    /* SWAP C */
    case 0x31: swap_8bit_reg(&g_cpu.regs.BC.l); break;
    /* SWAP D */
    case 0x32: swap_8bit_reg(&g_cpu.regs.DE.h); break;
    /* SWAP E */
    case 0x33: swap_8bit_reg(&g_cpu.regs.DE.l); break;
    /* SWAP H */
    case 0x34: swap_8bit_reg(&g_cpu.regs.HL.h); break;
    /* SWAP L */
    case 0x35: swap_8bit_reg(&g_cpu.regs.HL.l); break;
    /* SWAP (HL) */
    case 0x36: swap_8bit_address(g_cpu.regs.HL.word); break;
    /* SWAP A */
    case 0x37: swap_8bit_reg(&g_cpu.regs.AF.h); break;


    /** Shift right logic register r8 **/
    /* SRL B */
    case 0x38: srl_8bit_reg(&g_cpu.regs.BC.h); break;
    /* SRL C */
    case 0x39: srl_8bit_reg(&g_cpu.regs.BC.l); break;
    /* SRL D */
    case 0x3A: srl_8bit_reg(&g_cpu.regs.DE.h); break;
    /* SRL E */
    case 0x3B: srl_8bit_reg(&g_cpu.regs.DE.l); break;
    /* SRL H */
    case 0x3C: srl_8bit_reg(&g_cpu.regs.HL.h); break;
    /* SRL L */
    case 0x3D: srl_8bit_reg(&g_cpu.regs.HL.l); break;
    /* SRL (HL) */
    case 0x3E: srl_8bit_address(g_cpu.regs.HL.word); break;
    /* SRL A */
    case 0x3F: srl_8bit_reg(&g_cpu.regs.AF.h); break;


    /** Check bit in register r8 **/
    /* BIT 0, B */
    case 0x40: bit_check_8bit_reg(g_cpu.regs.BC.h, 0x01 << 0); break;
    /* BIT 0, C */
    case 0x41: bit_check_8bit_reg(g_cpu.regs.BC.l, 0x01 << 0); break;
    /* BIT 0, D */
    case 0x42: bit_check_8bit_reg(g_cpu.regs.DE.h, 0x01 << 0); break;
    /* BIT 0, E */
    case 0x43: bit_check_8bit_reg(g_cpu.regs.DE.l, 0x01 << 0); break;
    /* BIT 0, H */
    case 0x44: bit_check_8bit_reg(g_cpu.regs.HL.h, 0x01 << 0); break;
    /* BIT 0, L */
    case 0x45: bit_check_8bit_reg(g_cpu.regs.HL.l, 0x01 << 0); break;
    /* BIT 0, (HL) */
    case 0x46: bit_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 0); break;
    /* BIT 0, A */
    case 0x47: bit_check_8bit_reg(g_cpu.regs.AF.h, 0x01 << 0); break;

    /* BIT 1, B */
    case 0x48: bit_check_8bit_reg(g_cpu.regs.BC.h, 0x01 << 1); break;
    /* BIT 1, C */
    case 0x49: bit_check_8bit_reg(g_cpu.regs.BC.l, 0x01 << 1); break;
    /* BIT 1, D */
    case 0x4A: bit_check_8bit_reg(g_cpu.regs.DE.h, 0x01 << 1); break;
    /* BIT 1, E */
    case 0x4B: bit_check_8bit_reg(g_cpu.regs.DE.l, 0x01 << 1); break;
    /* BIT 1, H */
    case 0x4C: bit_check_8bit_reg(g_cpu.regs.HL.h, 0x01 << 1); break;
    /* BIT 1, L */
    case 0x4D: bit_check_8bit_reg(g_cpu.regs.HL.l, 0x01 << 1); break;
    /* BIT 1, (HL) */
    case 0x4E: bit_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 1); break;
    /* BIT 1, A */
    case 0x4F: bit_check_8bit_reg(g_cpu.regs.AF.h, 0x01 << 1); break;

    /* BIT 2, B */
    case 0x50: bit_check_8bit_reg(g_cpu.regs.BC.h, 0x01 << 2); break;
    /* BIT 2, C */
    case 0x51: bit_check_8bit_reg(g_cpu.regs.BC.l, 0x01 << 2); break;
    /* BIT 2, D */
    case 0x52: bit_check_8bit_reg(g_cpu.regs.DE.h, 0x01 << 2); break;
    /* BIT 2, E */
    case 0x53: bit_check_8bit_reg(g_cpu.regs.DE.l, 0x01 << 2); break;
    /* BIT 2, H */
    case 0x54: bit_check_8bit_reg(g_cpu.regs.HL.h, 0x01 << 2); break;
    /* BIT 2, L */
    case 0x55: bit_check_8bit_reg(g_cpu.regs.HL.l, 0x01 << 2); break;
    /* BIT 2, (HL) */
    case 0x56: bit_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 2); break;
    /* BIT 2, A */
    case 0x57: bit_check_8bit_reg(g_cpu.regs.AF.h, 0x01 << 2); break;

    /* BIT 3, B */
    case 0x58: bit_check_8bit_reg(g_cpu.regs.BC.h, 0x01 << 3); break;
    /* BIT 3, C */
    case 0x59: bit_check_8bit_reg(g_cpu.regs.BC.l, 0x01 << 3); break;
    /* BIT 3, D */
    case 0x5A: bit_check_8bit_reg(g_cpu.regs.DE.h, 0x01 << 3); break;
    /* BIT 3, E */
    case 0x5B: bit_check_8bit_reg(g_cpu.regs.DE.l, 0x01 << 3); break;
    /* BIT 3, H */
    case 0x5C: bit_check_8bit_reg(g_cpu.regs.HL.h, 0x01 << 3); break;
    /* BIT 3, L */
    case 0x5D: bit_check_8bit_reg(g_cpu.regs.HL.l, 0x01 << 3); break;
    /* BIT 3, (HL) */
    case 0x5E: bit_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 3); break;
    /* BIT 3, A */
    case 0x5F: bit_check_8bit_reg(g_cpu.regs.AF.h, 0x01 << 3); break;

    /* BIT 4, B */
    case 0x60: bit_check_8bit_reg(g_cpu.regs.BC.h, 0x01 << 4); break;
    /* BIT 4, C */
    case 0x61: bit_check_8bit_reg(g_cpu.regs.BC.l, 0x01 << 4); break;
    /* BIT 4, D */
    case 0x62: bit_check_8bit_reg(g_cpu.regs.DE.h, 0x01 << 4); break;
    /* BIT 4, E */
    case 0x63: bit_check_8bit_reg(g_cpu.regs.DE.l, 0x01 << 4); break;
    /* BIT 4, H */
    case 0x64: bit_check_8bit_reg(g_cpu.regs.HL.h, 0x01 << 4); break;
    /* BIT 4, L */
    case 0x65: bit_check_8bit_reg(g_cpu.regs.HL.l, 0x01 << 4); break;
    /* BIT 4, (HL) */
    case 0x66: bit_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 4); break;
    /* BIT 4, A */
    case 0x67: bit_check_8bit_reg(g_cpu.regs.AF.h, 0x01 << 4); break;

    /* BIT 5, B */
    case 0x68: bit_check_8bit_reg(g_cpu.regs.BC.h, 0x01 << 5); break;
    /* BIT 5, C */
    case 0x69: bit_check_8bit_reg(g_cpu.regs.BC.l, 0x01 << 5); break;
    /* BIT 5, D */
    case 0x6A: bit_check_8bit_reg(g_cpu.regs.DE.h, 0x01 << 5); break;
    /* BIT 5, E */
    case 0x6B: bit_check_8bit_reg(g_cpu.regs.DE.l, 0x01 << 5); break;
    /* BIT 5, H */
    case 0x6C: bit_check_8bit_reg(g_cpu.regs.HL.h, 0x01 << 5); break;
    /* BIT 5, L */
    case 0x6D: bit_check_8bit_reg(g_cpu.regs.HL.l, 0x01 << 5); break;
    /* BIT 5, (HL) */
    case 0x6E: bit_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 5); break;
    /* BIT 5, A */
    case 0x6F: bit_check_8bit_reg(g_cpu.regs.AF.h, 0x01 << 5); break;

    /* BIT 6, B */
    case 0x70: bit_check_8bit_reg(g_cpu.regs.BC.h, 0x01 << 6); break;
    /* BIT 6, C */
    case 0x71: bit_check_8bit_reg(g_cpu.regs.BC.l, 0x01 << 6); break;
    /* BIT 6, D */
    case 0x72: bit_check_8bit_reg(g_cpu.regs.DE.h, 0x01 << 6); break;
    /* BIT 6, E */
    case 0x73: bit_check_8bit_reg(g_cpu.regs.DE.l, 0x01 << 6); break;
    /* BIT 6, H */
    case 0x74: bit_check_8bit_reg(g_cpu.regs.HL.h, 0x01 << 6); break;
    /* BIT 6, L */
    case 0x75: bit_check_8bit_reg(g_cpu.regs.HL.l, 0x01 << 6); break;
    /* BIT 6, (HL) */
    case 0x76: bit_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 6); break;
    /* BIT 6, A */
    case 0x77: bit_check_8bit_reg(g_cpu.regs.AF.h, 0x01 << 6); break;

    /* BIT 7, B */
    case 0x78: bit_check_8bit_reg(g_cpu.regs.BC.h, 0x01 << 7); break;
    /* BIT 7, C */
    case 0x79: bit_check_8bit_reg(g_cpu.regs.BC.l, 0x01 << 7); break;
    /* BIT 7, D */
    case 0x7A: bit_check_8bit_reg(g_cpu.regs.DE.h, 0x01 << 7); break;
    /* BIT 7, E */
    case 0x7B: bit_check_8bit_reg(g_cpu.regs.DE.l, 0x01 << 7); break;
    /* BIT 7, H */
    case 0x7C: bit_check_8bit_reg(g_cpu.regs.HL.h, 0x01 << 7); break;
    /* BIT 7, L */
    case 0x7D: bit_check_8bit_reg(g_cpu.regs.HL.l, 0x01 << 7); break;
    /* BIT 7, (HL) */
    case 0x7E: bit_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 7); break;
    /* BIT 7, A */
    case 0x7F: bit_check_8bit_reg(g_cpu.regs.AF.h, 0x01 << 7); break;

    /** Reset bit in register r8 **/
    /* RES 0, B */
    case 0x80: res_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 0); break;
    /* RES 0, C */
    case 0x81: res_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 0); break;
    /* RES 0, D */
    case 0x82: res_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 0); break;
    /* RES 0, E */
    case 0x83: res_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 0); break;
    /* RES 0, H */
    case 0x84: res_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 0); break;
    /* RES 0, L */
    case 0x85: res_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 0); break;
    /* RES 0, (HL) */
    case 0x86: res_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 0); break;
    /* RES 0, A */
    case 0x87: res_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 0); break;

    /* RES 1, B */
    case 0x88: res_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 1); break;
    /* RES 1, C */
    case 0x89: res_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 1); break;
    /* RES 1, D */
    case 0x8A: res_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 1); break;
    /* RES 1, E */
    case 0x8B: res_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 1); break;
    /* RES 1, H */
    case 0x8C: res_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 1); break;
    /* RES 1, L */
    case 0x8D: res_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 1); break;
    /* RES 1, (HL) */
    case 0x8E: res_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 1); break;
    /* RES 1, A */
    case 0x8F: res_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 1); break;

    /* RES 2, B */
    case 0x90: res_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 2); break;
    /* RES 2, C */
    case 0x91: res_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 2); break;
    /* RES 2, D */
    case 0x92: res_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 2); break;
    /* RES 2, E */
    case 0x93: res_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 2); break;
    /* RES 2, H */
    case 0x94: res_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 2); break;
    /* RES 2, L */
    case 0x95: res_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 2); break;
    /* RES 2, (HL) */
    case 0x96: res_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 2); break;
    /* RES 2, A */
    case 0x97: res_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 2); break;

    /* RES 3, B */
    case 0x98: res_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 3); break;
    /* RES 3, C */
    case 0x99: res_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 3); break;
    /* RES 3, D */
    case 0x9A: res_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 3); break;
    /* RES 3, E */
    case 0x9B: res_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 3); break;
    /* RES 3, H */
    case 0x9C: res_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 3); break;
    /* RES 3, L */
    case 0x9D: res_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 3); break;
    /* RES 3, (HL) */
    case 0x9E: res_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 3); break;
    /* RES 3, A */
    case 0x9F: res_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 3); break;

    /* RES 4, B */
    case 0xA0: res_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 4); break;
    /* RES 4, C */
    case 0xA1: res_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 4); break;
    /* RES 4, D */
    case 0xA2: res_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 4); break;
    /* RES 4, E */
    case 0xA3: res_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 4); break;
    /* RES 4, H */
    case 0xA4: res_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 4); break;
    /* RES 4, L */
    case 0xA5: res_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 4); break;
    /* RES 4, (HL) */
    case 0xA6: res_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 4); break;
    /* RES 4, A */
    case 0xA7: res_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 4); break;

    /* RES 5, B */
    case 0xA8: res_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 5); break;
    /* RES 5, C */
    case 0xA9: res_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 5); break;
    /* RES 5, D */
    case 0xAA: res_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 5); break;
    /* RES 5, E */
    case 0xAB: res_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 5); break;
    /* RES 5, H */
    case 0xAC: res_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 5); break;
    /* RES 5, L */
    case 0xAD: res_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 5); break;
    /* RES 5, (HL) */
    case 0xAE: res_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 5); break;
    /* RES 5, A */
    case 0xAF: res_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 5); break;

    /* RES 6, B */
    case 0xB0: res_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 6); break;
    /* RES 6, C */
    case 0xB1: res_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 6); break;
    /* RES 6, D */
    case 0xB2: res_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 6); break;
    /* RES 6, E */
    case 0xB3: res_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 6); break;
    /* RES 6, H */
    case 0xB4: res_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 6); break;
    /* RES 6, L */
    case 0xB5: res_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 6); break;
    /* RES 6, (HL) */
    case 0xB6: res_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 6); break;
    /* RES 6, A */
    case 0xB7: res_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 6); break;

    /* RES 7, B */
    case 0xB8: res_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 7); break;
    /* RES 7, C */
    case 0xB9: res_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 7); break;
    /* RES 7, D */
    case 0xBA: res_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 7); break;
    /* RES 7, E */
    case 0xBB: res_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 7); break;
    /* RES 7, H */
    case 0xBC: res_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 7); break;
    /* RES 7, L */
    case 0xBD: res_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 7); break;
    /* RES 7, (HL) */
    case 0xBE: res_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 7); break;
    /* RES 7, A */
    case 0xBF: res_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 7); break;


    /** Set bit in register r8 **/
    /* SET 0, B */
    case 0xC0: set_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 0); break;
    /* SET 0, C */
    case 0xC1: set_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 0); break;
    /* SET 0, D */
    case 0xC2: set_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 0); break;
    /* SET 0, E */
    case 0xC3: set_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 0); break;
    /* SET 0, H */
    case 0xC4: set_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 0); break;
    /* SET 0, L */
    case 0xC5: set_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 0); break;
    /* SET 0, (HL) */
    case 0xC6: set_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 0); break;
    /* SET 0, A */
    case 0xC7: set_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 0); break;

    /* SET 1, B */
    case 0xC8: set_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 1); break;
    /* SET 1, C */
    case 0xC9: set_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 1); break;
    /* SET 1, D */
    case 0xCA: set_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 1); break;
    /* SET 1, E */
    case 0xCB: set_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 1); break;
    /* SET 1, H */
    case 0xCC: set_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 1); break;
    /* SET 1, L */
    case 0xCD: set_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 1); break;
    /* SET 1, (HL) */
    case 0xCE: set_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 1); break;
    /* SET 1, A */
    case 0xCF: set_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 1); break;

    /* SET 2, B */
    case 0xD0: set_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 2); break;
    /* SET 2, C */
    case 0xD1: set_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 2); break;
    /* SET 2, D */
    case 0xD2: set_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 2); break;
    /* SET 2, E */
    case 0xD3: set_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 2); break;
    /* SET 2, H */
    case 0xD4: set_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 2); break;
    /* SET 2, L */
    case 0xD5: set_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 2); break;
    /* SET 2, (HL) */
    case 0xD6: set_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 2); break;
    /* SET 2, A */
    case 0xD7: set_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 2); break;

    /* SET 3, B */
    case 0xD8: set_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 3); break;
    /* SET 3, C */
    case 0xD9: set_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 3); break;
    /* SET 3, D */
    case 0xDA: set_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 3); break;
    /* SET 3, E */
    case 0xDB: set_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 3); break;
    /* SET 3, H */
    case 0xDC: set_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 3); break;
    /* SET 3, L */
    case 0xDD: set_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 3); break;
    /* SET 3, (HL) */
    case 0xDE: set_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 3); break;
    /* SET 3, A */
    case 0xDF: set_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 3); break;

    /* SET 4, B */
    case 0xE0: set_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 4); break;
    /* SET 4, C */
    case 0xE1: set_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 4); break;
    /* SET 4, D */
    case 0xE2: set_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 4); break;
    /* SET 4, E */
    case 0xE3: set_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 4); break;
    /* SET 4, H */
    case 0xE4: set_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 4); break;
    /* SET 4, L */
    case 0xE5: set_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 4); break;
    /* SET 4, (HL) */
    case 0xE6: set_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 4); break;
    /* SET 4, A */
    case 0xE7: set_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 4); break;

    /* SET 5, B */
    case 0xE8: set_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 5); break;
    /* SET 5, C */
    case 0xE9: set_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 5); break;
    /* SET 5, D */
    case 0xEA: set_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 5); break;
    /* SET 5, E */
    case 0xEB: set_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 5); break;
    /* SET 5, H */
    case 0xEC: set_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 5); break;
    /* SET 5, L */
    case 0xED: set_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 5); break;
    /* SET 5, (HL) */
    case 0xEE: set_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 5); break;
    /* SET 5, A */
    case 0xEF: set_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 5); break;

    /* SET 6, B */
    case 0xF0: set_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 6); break;
    /* SET 6, C */
    case 0xF1: set_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 6); break;
    /* SET 6, D */
    case 0xF2: set_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 6); break;
    /* SET 6, E */
    case 0xF3: set_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 6); break;
    /* SET 6, H */
    case 0xF4: set_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 6); break;
    /* SET 6, L */
    case 0xF5: set_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 6); break;
    /* SET 6, (HL) */
    case 0xF6: set_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 6); break;
    /* SET 6, A */
    case 0xF7: set_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 6); break;

    /* SET 7, B */
    case 0xF8: set_check_8bit_reg(&g_cpu.regs.BC.h, 0x01 << 7); break;
    /* SET 7, C */
    case 0xF9: set_check_8bit_reg(&g_cpu.regs.BC.l, 0x01 << 7); break;
    /* SET 7, D */
    case 0xFA: set_check_8bit_reg(&g_cpu.regs.DE.h, 0x01 << 7); break;
    /* SET 7, E */
    case 0xFB: set_check_8bit_reg(&g_cpu.regs.DE.l, 0x01 << 7); break;
    /* SET 7, H */
    case 0xFC: set_check_8bit_reg(&g_cpu.regs.HL.h, 0x01 << 7); break;
    /* SET 7, L */
    case 0xFD: set_check_8bit_reg(&g_cpu.regs.HL.l, 0x01 << 7); break;
    /* SET 7, (HL) */
    case 0xFE: set_check_8bit_address(g_cpu.regs.HL.word, 0x01 << 7); break;
    /* SET 7, A */
    case 0xFF: set_check_8bit_reg(&g_cpu.regs.AF.h, 0x01 << 7); break;
  }
}
