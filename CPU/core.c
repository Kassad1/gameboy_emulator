#include "processor.h"
#include "mem.h"
#include "dbg.h"
cpu g_cpu;


/******************* ALU FLAGS HANDLERS ******************/
void flags_inc_dec(uint8_t prev_value, uint8_t curr_value, int state)
{
  g_cpu.regs.flag_z = (curr_value == 0x00) ? 1 : 0;
  g_cpu.regs.flag_n = (!state) ? 0 : 1;
  g_cpu.regs.flag_h = (prev_value & 0x0F) == 0x00 ? 1 : 0;
}

void flags_add(uint8_t result, uint8_t prev, uint8_t value)
{
  g_cpu.regs.flag_z = (result == 0) ? 1 : 0;

  g_cpu.regs.flag_n = 0;

  g_cpu.regs.flag_c = ((prev + value) > 0xFF) ? 1 : 0;

  g_cpu.regs.flag_h = (((prev & 0x0F) + (value & 0x0F)) > 0x0F) ? 1 : 0;
}

void flags_sub(uint8_t result, uint8_t prev, uint8_t value)
{
  g_cpu.regs.flag_z = (result == 0) ? 1 : 0;

  g_cpu.regs.flag_n = 1;

  g_cpu.regs.flag_c = (prev < value) ? 1 : 0;

  g_cpu.regs.flag_h = (((prev & 0x0F) - (value & 0x0F)) < 0) ? 1 : 0;
} 


void flags_and(void)
{
  g_cpu.regs.flag_z = (g_cpu.regs.AF.h == 0) ? 1 : 0;

  g_cpu.regs.flag_n = 0;

  g_cpu.regs.flag_h = 1;

  g_cpu.regs.flag_c = 0;
}


void flags_or(void)
{
  g_cpu.regs.flag_z = (g_cpu.regs.AF.h == 0) ? 1 : 0;

  g_cpu.regs.flag_n = 0;

  g_cpu.regs.flag_h = 0;

  g_cpu.regs.flag_c = 0;
}

void flags_xor(void)
{
  g_cpu.regs.flag_z = (g_cpu.regs.AF.h == 0) ? 1 : 0;

  g_cpu.regs.flag_n = 0;

  g_cpu.regs.flag_h = 0;

  g_cpu.regs.flag_c = 0;
}
/*********************************************************/

/******************** RR FLAGS HANDLERS ******************/

void flags_rotate(uint8_t value)
{
  g_cpu.regs.flag_n = 0;

  g_cpu.regs.flag_h = 0;

  g_cpu.regs.flag_z = (value == 0x00) ? 1 : 0;
}

/*********************************************************/

/****************** SWAP FLAGS HANDLERS ******************/
void flags_swap(uint8_t value)
{
  g_cpu.regs.flag_z = (value == 0x00) ? 1 : 0;

  g_cpu.regs.flag_n = 0;

  g_cpu.regs.flag_h = 0;

  g_cpu.regs.flag_c = 0;
}
/*********************************************************/
int processor_init(void)
{
  g_cpu.regs.PC.word = 0x0100;
  g_cpu.regs.AF.word = 0x0100;
  g_cpu.halt = false;
  //g_cpu.regs.AF.h = 0x01;
  return 0;
}


int cpu_step(void)
{
  /* Following the pipeline Fetch Decode and Execute */
  if (!g_cpu.halt) {
    printf("%04x: ", g_cpu.regs.PC.word);
    char flags[16];
    sprintf(flags, "%c%c%c%c",
	    g_cpu.regs.flag_z ? 'Z' : '-',
	    g_cpu.regs.flag_n ? 'N' : '-',
	    g_cpu.regs.flag_h ? 'H' : '-',
	    g_cpu.regs.flag_c ? 'C' : '-'
	    );
    g_cpu.opcode = read_address(g_cpu.regs.PC.word++);
    printf("%02X %02X %02X AF:%04X BC:%04X DE:%04X HL:%04X SP: %04X FLAGS:%s\n",
	   g_cpu.opcode,
	   read_address(g_cpu.regs.PC.word), read_address(g_cpu.regs.PC.word + 1),
	   g_cpu.regs.AF.word, g_cpu.regs.BC.word, g_cpu.regs.DE.word,
	   g_cpu.regs.HL.word, g_cpu.regs.SP.word, flags);

    
    dbg_update();
    dbg_print();

    execute_next();
    //sleep(1);
  }
  return 0;
}
