#include "processor.h"
#include "mem.h"
#include "interrupt.h"

uint16_t stack_pop(void)
{
  uint16_t tmp = (read_address(g_cpu.regs.SP.word + 1) << 8) | (read_address(g_cpu.regs.SP.word));
  
  g_cpu.regs.SP.word += 2;

  /*
  for (int i = 0; i < 128; i++) {
    if (g_ram.h_ram[i] != 0x00)
      printf("%d, %02X\n", i, g_ram.h_ram[i]);
  }
  */
  
  g_cpu.cycle += 5;
  
  return tmp;
}

void stack_push(uint16_t value)
{
  uint8_t value_hi = ((value >> 8)) & 0xFF;
  uint8_t value_lo = value & 0xFF;
  g_cpu.regs.SP.word--;
  write_address(g_cpu.regs.SP.word, value_hi);
  g_cpu.regs.SP.word--;
  write_address(g_cpu.regs.SP.word, value_lo);
  g_cpu.cycle += 5;
}

static void stop(void)
{
  g_cpu.cycle += 1;
}

static void jump_address(bool state, bool flag, bool condition)
{
  uint16_t tmp = read_address(g_cpu.regs.PC.word + 1) << 8 | read_address(g_cpu.regs.PC.word);
  g_cpu.regs.PC.word += 2;
  if (state || flag == condition){
    g_cpu.regs.PC.word = tmp;
    g_cpu.cycle += 4;
    return;
  }
  if (!state && flag != condition)
    g_cpu.cycle += 3;
  
}

static void jump_immediate(bool state, bool flag, bool condition)
{
  int8_t n = read_address(g_cpu.regs.PC.word);
  printf("%d\n", n);
  /* Maybe come back later to conside what if its beyond 0xFFFF */
  if(state || flag == condition){
    g_cpu.regs.PC.word += n;
    g_cpu.cycle += 3;
  }
  if (!state && flag != condition)
    g_cpu.cycle += 2;

  g_cpu.regs.PC.word++;
}

static void call_address(bool state, bool flag, bool condition)
{
  uint16_t tmp = read_address(g_cpu.regs.PC.word + 1) << 8;
  tmp |= read_address(g_cpu.regs.PC.word);
  g_cpu.regs.PC.word += 2;
  if (state || flag == condition){
    stack_push(g_cpu.regs.PC.word);
    g_cpu.regs.PC.word = tmp;
    g_cpu.cycle += 6;
  }

  if (!state && flag != condition)
    g_cpu.cycle += 3;
}

static void ret_address(bool state, bool flag, bool condition)
{  
  if (state || flag == condition){
    g_cpu.regs.PC.word = stack_pop();
    return;
  }

  if (!state && flag != condition)
    g_cpu.cycle += 3;
}

static void cpu_reset(uint8_t n)
{
  stack_push(g_cpu.regs.PC.word);
  g_cpu.regs.PC.word = n;
}

static void interrupt_state(bool state)
{
  g_cpu.cycle += 1;  
  cpu_interrupt.master = state;
}

static void DAA(void)
{
  if (!g_cpu.regs.flag_n) {
    if (g_cpu.regs.flag_h || (g_cpu.regs.AF.h & 0x0F) > 0x09) {
      g_cpu.regs.AF.h += 0x60;
      g_cpu.regs.flag_c = (g_cpu.regs.AF.h & 0xF0) == 0xF0 ? 1 : 0;
    }
    if ((g_cpu.regs.AF.h & 0xF0) > 0x90 || g_cpu.regs.flag_c)
      g_cpu.regs.AF.h += 0x60;
  } else {
    if (g_cpu.regs.flag_h || (g_cpu.regs.AF.h & 0x0F) > 0x09) {
      g_cpu.regs.AF.h -= 0x60;
      g_cpu.regs.flag_c = (g_cpu.regs.AF.h & 0xF0) == 0xF0 ? 1 : 0;
    }
    if ((g_cpu.regs.AF.h & 0xF0) > 0x90 || g_cpu.regs.flag_c)
      g_cpu.regs.AF.h -= 0x60;
  }

  g_cpu.regs.flag_z = (g_cpu.regs.AF.h == 0x00) ? 1 : 0;
}

static void load_8bit_iv(uint8_t *reg)
{
  *reg = read_address(g_cpu.regs.PC.word);
  g_cpu.regs.PC.word++;
  g_cpu.cycle += 2;
}

static void load_8bit_reg(uint8_t *reg1, uint8_t reg2)
{
  *reg1 = reg2;
  g_cpu.cycle += 1;
}

static void load_8bit_address(uint8_t *reg, uint16_t address)
{
  *reg = read_address(address);
  g_cpu.cycle += 1;
}

static void load_address_reg(reg *regs)
{
  /*
  regs->h = read_address(g_cpu.regs.PC.word++);
  regs->l = read_address(g_cpu.regs.PC.word++);
  */
  regs->l = read_address(g_cpu.regs.PC.word);
  g_cpu.regs.PC.word++;
  regs->h = read_address(g_cpu.regs.PC.word);
  g_cpu.regs.PC.word++;
}

static void inc_8bit_reg(uint8_t *reg)
{
  uint8_t prev_value = *reg;
  *reg += 1;
  
  g_cpu.regs.flag_z = (*reg == 0) ? 1 : 0;
  g_cpu.regs.flag_n = 0;
  g_cpu.regs.flag_h = (prev_value & 0x0F) == 0x0F ? 1 : 0;
  
  g_cpu.cycle += 1;
}

static void inc_8bit_address(uint16_t address)
{
  uint8_t prev_value = read_address(address);
  uint8_t curr_value = prev_value + 1;
  write_address(address, curr_value);

  g_cpu.regs.flag_z = (curr_value == 0) ? 1 : 0;
  g_cpu.regs.flag_n = 0;
  g_cpu.regs.flag_h = (prev_value & 0x0F) == 0x0F ? 1 : 0;

    
  g_cpu.cycle += 3;
}

static void dec_8bit_reg(uint8_t *reg)
{
  uint8_t prev_value = *reg;
  *reg -= 1;
  
  g_cpu.regs.flag_z = (*reg == 0) ? 1 : 0;
  g_cpu.regs.flag_n = 1;
  g_cpu.regs.flag_h = (prev_value & 0x0F) == 0 ? 1 : 0;
  
  g_cpu.cycle += 1;
}

static void dec_8bit_address(uint16_t address)
{
  uint8_t prev_value = read_address(address);
  uint8_t curr_value = prev_value - 1;
  write_address(address, curr_value);

  g_cpu.regs.flag_z = (curr_value == 0) ? 1 : 0;
  g_cpu.regs.flag_n = 1;
  g_cpu.regs.flag_h = (prev_value & 0x0F) == 0 ? 1 : 0;

  
  g_cpu.cycle += 3;
}

static void add_8bit_reg(uint8_t *reg, uint8_t value)
{
  uint8_t prev = *reg;

  *reg += value;

  flags_add(*reg, prev, value);
}

static void add_8bit_address(uint8_t *reg, uint16_t address)
{
  uint8_t value = read_address(address);
  uint8_t prev = *reg;

  *reg += value;

  flags_add(*reg, prev, value);
}

static void sub_8bit_reg(uint8_t *reg, uint8_t value)
{
  uint8_t prev = *reg;

  *reg -= value;

  flags_sub(*reg, prev, value);
}

static void sub_8bit_address(uint8_t *reg, uint16_t address)
{
  uint8_t prev = *reg;
  uint8_t value = read_address(address);

  *reg -= value;

  flags_sub(*reg, prev, value);
}


static void inc_16bit(uint16_t *reg)
{
  *reg += 1;

  g_cpu.cycle += 1;
}

static void dec_16bit(uint16_t *reg)
{
  *reg -= 1;

  g_cpu.cycle += 1;
}

static void add_16bit(uint16_t *reg, uint16_t value)
{
  uint16_t prev_value = *reg;

  *reg += value;

  g_cpu.regs.flag_n = 0;

  g_cpu.regs.flag_c = (prev_value + value) > 0xFFFF ? 1 : 0;

  g_cpu.regs.flag_h = ((prev_value & 0xFF00) & 0x0F) + ((value >> 8) & 0x0F) ? 1 : 0;

  //g_cpu.regs.SP.word += 2;
}

static void add_sp(uint16_t value)
{
  //  uint16_t prev_value = g_cpu.regs.SP.word;

  g_cpu.regs.SP.word += value;

  g_cpu.regs.flag_z = 0;

  g_cpu.regs.flag_n = 0;

  g_cpu.regs.flag_c = (value > 0xFFFF) ? 1 : 0;

  g_cpu.regs.flag_h = ((g_cpu.regs.SP.word & 0x0F) + (value & 0x0F)) > 0x0F ? 1 : 0;
  
  //g_cpu.regs.SP.word += 4;
}

static void and_8bit_reg(uint8_t reg)
{

  g_cpu.regs.AF.h = g_cpu.regs.AF.h & reg;

  flags_and();

  g_cpu.cycle += 1;
}

static void and_8bit_address(uint16_t address)
{

  g_cpu.regs.AF.h = g_cpu.regs.AF.h & read_address(address);

  flags_and();

  g_cpu.cycle += 2;
}

static void or_8bit_reg(uint8_t reg)
{
  g_cpu.regs.AF.h = g_cpu.regs.AF.h | reg;

  flags_or();

  g_cpu.cycle += 1;
}

static void or_8bit_address(uint16_t address)
{
  g_cpu.regs.AF.h = g_cpu.regs.AF.h | read_address(address);

  flags_or();

  g_cpu.cycle += 2;
}

static void xor_8bit_reg(uint8_t reg)
{
  g_cpu.regs.AF.h ^= reg;

  flags_xor();

  g_cpu.cycle += 1;
}

static void xor_8bit_address(uint16_t address)
{
  g_cpu.regs.AF.h ^= read_address(address);

  flags_xor();

  g_cpu.cycle += 2;
}

static void cp_8bit_reg(uint8_t value)
{
  g_cpu.regs.flag_n = 1;

  if((g_cpu.regs.AF.h - value) == 0)
    g_cpu.regs.flag_z = 1;

  int8_t half_c_check = (g_cpu.regs.AF.h & 0x0F) - (value & 0x0F);

  if(half_c_check < 0)
    g_cpu.regs.flag_h = 1;

  if(g_cpu.regs.AF.h < value)
    g_cpu.regs.flag_c = 1;

  g_cpu.cycle += 1;
}

static void cp_8bit_address(uint16_t address)
{
  uint8_t value = read_address(address);
  g_cpu.regs.flag_n = 1;

  if((g_cpu.regs.AF.h - value) == 0)
    g_cpu.regs.flag_z = 1;

  int16_t half_c_check = (g_cpu.regs.AF.h & 0x0F) - (value & 0x0F);

  if(half_c_check < 0)
    g_cpu.regs.flag_h = 1;

  if(g_cpu.regs.AF.h < value)
    g_cpu.regs.flag_c = 1;

  g_cpu.cycle += 2;
}

static void rlca_8bit(void)
{
  bool bit_set = g_cpu.regs.AF.h & 0x80;
  g_cpu.regs.AF.h <<= 1;
  
  if(bit_set) {
    g_cpu.regs.flag_c = 1;
    g_cpu.regs.AF.h |= 0x01;
  }
  
  flags_rotate(g_cpu.regs.AF.h);
}

static void rla_8bit(void)
{
  bool bit_set = g_cpu.regs.AF.h & 0x80;
  bool carry = g_cpu.regs.flag_c;

  g_cpu.regs.AF.h <<= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  if(carry)
    g_cpu.regs.AF.h |= 0x01;

  flags_rotate(g_cpu.regs.AF.h);
}

static void rrca_8bit(void)
{
  /* Moving the bit to RSB */
  bool bit_set = g_cpu.regs.AF.h & 0x01;
  g_cpu.regs.AF.h >>= 1;
  
  if(bit_set) {
    g_cpu.regs.flag_c = 1;
    g_cpu.regs.AF.h |= 0x80;
  }
  
  flags_rotate(g_cpu.regs.AF.h);
}

static void rra_8bit(void)
{
  bool bit_set = g_cpu.regs.AF.h & 0x10;
  bool carry = g_cpu.regs.flag_c;

  g_cpu.regs.AF.h >>= 1;

  if(bit_set)
    g_cpu.regs.flag_c = 1;

  if(carry)
    g_cpu.regs.AF.h |= 0x80;

  flags_rotate(g_cpu.regs.AF.h);
}

  /* Types: 
   * u8: 8-bit unsigned int
   * i8: 8-bit signed int
   * u16: 16-bit unsigned int
   */

void execute_next(void)
{
  uint8_t opcode = g_cpu.opcode, value;
  uint16_t immediate_value, address, value1;
  switch(opcode){
  /******** BRANCH ********/
  /* NOP */
  case 0x00: break;
  /* STOP */
  case 0x10: stop(); break;
  /* DAA */
  case 0x27: DAA(); break;
  /* SCF */
  case 0x37: g_cpu.regs.flag_c = 1; g_cpu.regs.flag_n = 0; g_cpu.regs.flag_h = 0; break;
  /* CPL */
  case 0x2F: g_cpu.regs.AF.h ^= 0xFF; g_cpu.regs.flag_n = 1; g_cpu.regs.flag_h = 1; break;
  /* CCF */
  case 0x3F: g_cpu.regs.flag_c = (g_cpu.regs.flag_c) ? 0 : 1; g_cpu.regs.flag_n = 0; g_cpu.regs.flag_h = 0;  break;
  /* DI */
  case 0xF3: interrupt_state(false); break;
  /* EI */
  case 0xFB: interrupt_state(true); break;
  /* HALT */
  case 0x76: g_cpu.halt = true; break;
  /******** MISC ********/
  /** Jump to address nn **/
  /* JP u16 */
  case 0xC3: jump_address(true, false, false); break; 
  /* JP NZ, u16 */
  case 0xC2: jump_address(false, g_cpu.regs.flag_z, false); break;
  /* JP Z, u16 */
  case 0xCA: jump_address(false, g_cpu.regs.flag_z, true); break;
  /* JP NC, u16 */
  case 0xD2: jump_address(false, g_cpu.regs.flag_c, false); break;
  /* JP C, u16 */
  case 0xDA: jump_address(false, g_cpu.regs.flag_c, true); break;
  /* JP HL */
  case 0xE9: g_cpu.regs.PC.word = g_cpu.regs.HL.word; break;
  /** JUMPING to an address with adding **/
  /* JR i8 */
  case 0x18: jump_immediate(true, false, false); break;
  /* JR NZ, i8 */
  case 0x20: jump_immediate(false, g_cpu.regs.flag_z, false); break;
  /* JR Z, i8 */
  case 0x28: jump_immediate(false, g_cpu.regs.flag_z, true); break;
  /* JR NC, i8 */
  case 0x30: jump_immediate(false, g_cpu.regs.flag_c, false); break;
  /* JR C, i8 */
  case 0x38: jump_immediate(false, g_cpu.regs.flag_c, true); break;
  /** Calls **/
  /* CALL u16 */
  case 0xCD: call_address(true, false, false); break;
  /* CALL NZ, u16 */
  case 0xC4: call_address(false, g_cpu.regs.flag_z, false); break;
  /* CALL Z, u16 */
  case 0xCC: call_address(false, g_cpu.regs.flag_z, true); break;
  /* CALL NC, u16 */
  case 0xD4: call_address(false, g_cpu.regs.flag_c, false); break;
  /* CALL C, u16 */
  case 0xDC: call_address(false, g_cpu.regs.flag_c, true); break;
  /** Return if true **/
  /* RET */
  case 0xC9: ret_address(true, false, false); break;
  /* RET NZ */
  case 0xC0: ret_address(false, g_cpu.regs.flag_z, false); break;
  /* RET Z */
  case 0xC8: ret_address(false, g_cpu.regs.flag_z, true); break;
  /* RET NC */
  case 0xD0: ret_address(false, g_cpu.regs.flag_c, false); break;
  /* RET C */
  case 0xD8: ret_address(false, g_cpu.regs.flag_c, true); break;
    
  /* RST 00h */
  case 0xC7: cpu_reset(0x00); break;
  /* RST 08h */
  case 0xCF: cpu_reset(0x08); break;
  /* RST 10h */
  case 0xD7: cpu_reset(0x10); break;
  /* RST 18h */
  case 0xDF: cpu_reset(0x18); break;
  /* RST 20h */
  case 0xE7: cpu_reset(0x20); break;
  /* RST 28h */
  case 0xEF: cpu_reset(0x28); break;
  /* RST 30h */
  case 0xF7: cpu_reset(0x30); break;
  /* RST 38h */
  case 0xFF: cpu_reset(0x38); break;
 
  /* RSTI */
  case 0xD9: g_cpu.regs.PC.word = stack_pop(); cpu_interrupt.enable = true; break;
    
  /******** 8-bit Load/Store/Move ********/
  /** Put value nn into n **/
  /* LD B, u8 */
  case 0x06: load_8bit_iv(&g_cpu.regs.BC.h); break;
  /* LD C, u8 */
  case 0x0E: load_8bit_iv(&g_cpu.regs.BC.l); break;
  /* LD D, u8 */
  case 0x16: load_8bit_iv(&g_cpu.regs.DE.h); break;
  /* LD E, u8 */
  case 0x1E: load_8bit_iv(&g_cpu.regs.DE.l); break;
  /* LD H, u8 */
  case 0x26: load_8bit_iv(&g_cpu.regs.HL.h); break;
  /* LD L, u8 */
  case 0x2E: load_8bit_iv(&g_cpu.regs.HL.l); break;
  /** Put value r2 into r1 **/
  /* LD A, A */
  case 0x7F: load_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.AF.h); break;
  /* LD A, B */
  case 0x78: load_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.BC.h); break;
  /* LD A, C */
  case 0x79: load_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.BC.l); break;
  /* LD A, D */
  case 0x7A: load_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.DE.h); break;
  /* LD A, E */
  case 0x7B: load_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.DE.l); break;
  /* LD A, H */
  case 0x7C: load_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.HL.h); break;
  /* LD A, L */
  case 0x7D: load_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.HL.l); break;
  /* LD A, (HL) */
  case 0x7E: load_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.HL.word); break;

    
  /* LD B, B */
  case 0x40: load_8bit_reg(&g_cpu.regs.BC.h, g_cpu.regs.BC.h); break;
  /* LD B, C */
  case 0x41: load_8bit_reg(&g_cpu.regs.BC.h, g_cpu.regs.BC.l); break;
  /* LD B, D */
  case 0x42: load_8bit_reg(&g_cpu.regs.BC.h, g_cpu.regs.DE.h); break;
  /* LD B, E */
  case 0x43: load_8bit_reg(&g_cpu.regs.BC.h, g_cpu.regs.DE.l); break;
  /* LD B, H */
  case 0x44: load_8bit_reg(&g_cpu.regs.BC.h, g_cpu.regs.HL.h); break;
  /* LD B, L */
  case 0x45: load_8bit_reg(&g_cpu.regs.BC.h, g_cpu.regs.HL.l); break;
  /* LD B, (HL) */
  case 0x46: load_8bit_address(&g_cpu.regs.BC.h, g_cpu.regs.HL.word); break;


  /* LD C, B */
  case 0x48: load_8bit_reg(&g_cpu.regs.BC.l, g_cpu.regs.BC.h); break;
  /* LD C, C */
  case 0x49: load_8bit_reg(&g_cpu.regs.BC.l, g_cpu.regs.BC.l); break;
  /* LD C, D */
  case 0x4A: load_8bit_reg(&g_cpu.regs.BC.l, g_cpu.regs.DE.h); break;
  /* LD C, E */
  case 0x4B: load_8bit_reg(&g_cpu.regs.BC.l, g_cpu.regs.DE.l); break;
  /* LD C, H */
  case 0x4C: load_8bit_reg(&g_cpu.regs.BC.l, g_cpu.regs.HL.h); break;
  /* LD C, L */
  case 0x4D: load_8bit_reg(&g_cpu.regs.BC.l, g_cpu.regs.HL.l); break; 
  /* LD C, (HL) */
  case 0x4E: load_8bit_address(&g_cpu.regs.BC.l, g_cpu.regs.HL.word); break;


  /* LD D, B */
  case 0x50: load_8bit_reg(&g_cpu.regs.DE.h, g_cpu.regs.BC.h); break;
  /* LD D, C */
  case 0x51: load_8bit_reg(&g_cpu.regs.DE.h, g_cpu.regs.BC.l); break;
  /* LD D, D */
  case 0x52: load_8bit_reg(&g_cpu.regs.DE.h, g_cpu.regs.DE.h); break;
  /* LD D, E */
  case 0x53: load_8bit_reg(&g_cpu.regs.DE.h, g_cpu.regs.DE.l); break;
  /* LD D, H */
  case 0x54: load_8bit_reg(&g_cpu.regs.DE.h, g_cpu.regs.HL.h); break;
  /* LD D, L */
  case 0x55: load_8bit_reg(&g_cpu.regs.DE.h, g_cpu.regs.HL.l); break;
  /* LD D, (HL) */
  case 0x56: load_8bit_address(&g_cpu.regs.DE.h, g_cpu.regs.HL.word); break;


  /* LD E, B */
  case 0x58: load_8bit_reg(&g_cpu.regs.DE.l, g_cpu.regs.BC.h); break;
  /* LD E, C */
  case 0x59: load_8bit_reg(&g_cpu.regs.DE.l, g_cpu.regs.BC.l); break;
  /* LD E, D */
  case 0x5A: load_8bit_reg(&g_cpu.regs.DE.l, g_cpu.regs.DE.h); break;
  /* LD E, E */
  case 0x5B: load_8bit_reg(&g_cpu.regs.DE.l, g_cpu.regs.DE.l); break;
  /* LD E, H */
  case 0x5C: load_8bit_reg(&g_cpu.regs.DE.l, g_cpu.regs.HL.h); break;
  /* LD E, L */
  case 0x5D: load_8bit_reg(&g_cpu.regs.DE.l, g_cpu.regs.HL.l); break;
  /* LD E, (HL) */
  case 0x5E: load_8bit_address(&g_cpu.regs.DE.l, g_cpu.regs.HL.word); break;


  /* LD H, B */
  case 0x60: load_8bit_reg(&g_cpu.regs.HL.h, g_cpu.regs.BC.h); break;
  /* LD H, C */
  case 0x61: load_8bit_reg(&g_cpu.regs.HL.h, g_cpu.regs.BC.l); break;
  /* LD H, D */
  case 0x62: load_8bit_reg(&g_cpu.regs.HL.h, g_cpu.regs.DE.h); break;
  /* LD H, E */
  case 0x63: load_8bit_reg(&g_cpu.regs.HL.h, g_cpu.regs.DE.l); break;
  /* LD H, H */
  case 0x64: load_8bit_reg(&g_cpu.regs.HL.h, g_cpu.regs.HL.h); break;
  /* LD H, L */
  case 0x65: load_8bit_reg(&g_cpu.regs.HL.h, g_cpu.regs.HL.l); break;
  /* LD H, (HL) */
  case 0x66: load_8bit_address(&g_cpu.regs.HL.h, g_cpu.regs.HL.word); break;


  /* LD L, B */
  case 0x68: load_8bit_reg(&g_cpu.regs.HL.l, g_cpu.regs.BC.h); break;
  /* LD L, C */
  case 0x69: load_8bit_reg(&g_cpu.regs.HL.l, g_cpu.regs.BC.l); break;
  /* LD L, D */
  case 0x6A: load_8bit_reg(&g_cpu.regs.HL.l, g_cpu.regs.DE.h); break;
  /* LD L, E */
  case 0x6B: load_8bit_reg(&g_cpu.regs.HL.l, g_cpu.regs.DE.l); break;
  /* LD L, H */
  case 0x6C: load_8bit_reg(&g_cpu.regs.HL.l, g_cpu.regs.HL.h); break;
  /* LD L, L */
  case 0x6D: load_8bit_reg(&g_cpu.regs.HL.l, g_cpu.regs.HL.l); break; 
  /* LD L, (HL) */
  case 0x6E: load_8bit_address(&g_cpu.regs.HL.l, g_cpu.regs.HL.word); break;

    
  /* LD (HL), B */
  case 0x70: write_address(g_cpu.regs.HL.word, g_cpu.regs.BC.h); g_cpu.cycle += 3; break;
  /* LD (HL), C */
  case 0x71: write_address(g_cpu.regs.HL.word, g_cpu.regs.BC.l); g_cpu.cycle += 3; break;
  /* LD (HL), D */
  case 0x72: write_address(g_cpu.regs.HL.word, g_cpu.regs.BC.h); g_cpu.cycle += 3; break;
  /* LD (HL), E */
  case 0x73: write_address(g_cpu.regs.HL.word, g_cpu.regs.BC.h); g_cpu.cycle += 3; break;
  /* LD (HL), H */
  case 0x74: write_address(g_cpu.regs.HL.word, g_cpu.regs.BC.h); g_cpu.cycle += 3; break;
  /* LD (HL), L */
  case 0x75: write_address(g_cpu.regs.HL.word, g_cpu.regs.BC.h); g_cpu.cycle += 3; break;
  /* LD (HL), u8 */
  case 0x36: write_address(g_cpu.regs.HL.word, g_cpu.regs.BC.h); g_cpu.cycle += 3; break;


  /** Put value n into A **/
  /* LD A, (BC) */
  case 0x0A: load_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.BC.word); break;
  /* LD A, (DE) */
  case 0x1A: load_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.DE.word); break;
  /* LD A, (nn) */
  case 0xFA:
    immediate_value = (0x00FF & g_cpu.regs.PC.word);
    g_cpu.regs.PC.word++;
    immediate_value |= (0xFF00 & g_cpu.regs.PC.word);
    g_cpu.regs.PC.word++;
    load_8bit_address(&g_cpu.regs.AF.h, immediate_value); break;
  /* LD A, u8 */
  case 0x3E: load_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.PC.word++); break;

  /** Put value A into n **/
  /* LD B, A */
  case 0x47: load_8bit_reg(&g_cpu.regs.BC.h, g_cpu.regs.AF.h); break;
  /* LD C, A */
  case 0x4F: load_8bit_reg(&g_cpu.regs.BC.l, g_cpu.regs.AF.h); break;
  /* LD D, A */
  case 0x57: load_8bit_reg(&g_cpu.regs.DE.h, g_cpu.regs.AF.h); break;
  /* LD E, A */
  case 0x5F: load_8bit_reg(&g_cpu.regs.DE.l, g_cpu.regs.AF.h); break;
  /* LD H, A */
  case 0x67: load_8bit_reg(&g_cpu.regs.HL.h, g_cpu.regs.AF.h); break;
  /* LD L, A */
  case 0x6F: load_8bit_reg(&g_cpu.regs.HL.l, g_cpu.regs.AF.h); break;
  /* LD (BC), A */
  case 0x02: write_address(g_cpu.regs.BC.word, g_cpu.regs.AF.h); g_cpu.cycle += 3; break;
  /* LD (DE), A */
  case 0x12: write_address(g_cpu.regs.DE.word, g_cpu.regs.AF.h); g_cpu.cycle += 3; break;
  /* LD (HL), A */ 
  case 0x77: write_address(g_cpu.regs.HL.word, g_cpu.regs.AF.h); g_cpu.cycle += 3; break; 
  /* LD (nn), A */ 
  case 0xEA:
    immediate_value = (0x00FF & g_cpu.regs.PC.word);
    g_cpu.regs.PC.word++;
    immediate_value |= (0xFF00 & g_cpu.regs.PC.word);
    g_cpu.regs.PC.word++;
    write_address(immediate_value, g_cpu.regs.AF.h); g_cpu.cycle += 3; break;


  /** Put value at address $FF00 + C into A **/
  /* LD A, (C) */
  case 0xF2: load_8bit_address(&g_cpu.regs.AF.h, 0xFF00 + g_cpu.regs.BC.l); break;
  
  
  /** Put A into address $FF00 + C **/
  /* LD (C), A */
  case 0xE2: write_address(0xFF00 + g_cpu.regs.BC.l, g_cpu.regs.AF.h); g_cpu.cycle += 2; break;

  
  /** Put value at address HL into A. Dec HL **/
  /* LD A, (HLD) */
  case 0x3A: load_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.HL.word); g_cpu.regs.HL.word--; break;
  
  
  /** Put A into memory address HL. Dec HL **/
  /* LD (HLD), A */
  case 0x32: write_address(g_cpu.regs.HL.word, g_cpu.regs.AF.h); g_cpu.regs.HL.word--; break;


  /** Put value at address HL into A. Inc HL **/
  /* LD A, (HLI) */
  case 0x2A: load_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.HL.word); g_cpu.regs.HL.word++; break;
  /* LD (HLI), A */
  case 0x22: write_address(g_cpu.regs.HL.word, g_cpu.regs.AF.h); g_cpu.regs.HL.word++; break;
  /* LD ($FF00 + n), A */
  case 0xE0:
      address = 0xFF00 | read_address(g_cpu.regs.PC.word++);
      write_address(address, g_cpu.regs.AF.h); g_cpu.cycle += 3; break;


  /** Put memory address $FF00 + n into A **/
  /* LD A, ($FF00 + n) */
  case 0xF0:
      address = 0xFF00 | read_address(g_cpu.regs.PC.word++);
      load_8bit_address(&g_cpu.regs.AF.h, address); break;

  /******** 16-bit Load/Store/Move ********/
  /** Loads address 16 into n **/
  /* LD BC, u16 */
  case 0x01: load_address_reg(&g_cpu.regs.BC); break;
  /* LD DE, u16 */
  case 0x11: load_address_reg(&g_cpu.regs.DE); break;
  /* LD HL, u16 */
  case 0x21: load_address_reg(&g_cpu.regs.HL); break;
  /* LD SP, u16 */
  case 0x31: load_address_reg(&g_cpu.regs.SP); break;


  /*************** Stack  ***************/    
  /** Put.SP.word value at address n **/
  /* LD (u16), SP */
  case 0x08:
    write_address(g_cpu.regs.PC.word, g_cpu.regs.SP.l);
    write_address(g_cpu.regs.PC.word + 1, g_cpu.regs.SP.h);
    g_cpu.regs.PC.word += 2;
    g_cpu.cycle += 5;
    break;

    
  /** POP two bytes off stack into nn **/
  /* POP AF */
  case 0xF1: g_cpu.regs.AF.word = stack_pop(); break;
  /* POP BC */
  case 0xC1: g_cpu.regs.BC.word = stack_pop(); break; 
  /* POP DE */
  case 0xD1: g_cpu.regs.DE.word = stack_pop(); break;
  /* POP HL */
  case 0xE1: g_cpu.regs.HL.word = stack_pop(); break;


  /** PUSH two bytes into stack from nn **/
  /* PUSH AF */
  case 0xF5: stack_push(g_cpu.regs.AF.word); break;
  /* PUSH BC */
  case 0xC5: stack_push(g_cpu.regs.BC.word); break;
  /* PUSH DE */
  case 0xD5: stack_push(g_cpu.regs.DE.word); break; 
  /* PUSH HL */
  case 0xE5: stack_push(g_cpu.regs.HL.word); break;

  /******** 8-bit ALU ********/
  /** Increment register n **/
  /* INC A */
  case 0x3C: inc_8bit_reg(&g_cpu.regs.AF.h); break;
  /* INC B */
  case 0x04: inc_8bit_reg(&g_cpu.regs.BC.h); break;
  /* INC C */
  case 0x0C: inc_8bit_reg(&g_cpu.regs.BC.l); break;
  /* INC D */
  case 0x14: inc_8bit_reg(&g_cpu.regs.DE.h); break;
  /* INC E */
  case 0x1C: inc_8bit_reg(&g_cpu.regs.DE.l); break;
  /* INC H */
  case 0x24: inc_8bit_reg(&g_cpu.regs.HL.h); break;
  /* INC L */
  case 0x2C: inc_8bit_reg(&g_cpu.regs.HL.l); break;
  /* INC (HL) */
  case 0x34: inc_8bit_address(g_cpu.regs.HL.word); break;
    

  /** Decrement register n **/
  /* DEC A */
  case 0x3D: dec_8bit_reg(&g_cpu.regs.AF.h); break;
  /* DEC B */
  case 0x05: dec_8bit_reg(&g_cpu.regs.BC.h); break;
  /* DEC C */
  case 0x0D: dec_8bit_reg(&g_cpu.regs.BC.l); break;
  /* DEC D */
  case 0x15: dec_8bit_reg(&g_cpu.regs.DE.h); break;
  /* DEC E */
  case 0x1D: dec_8bit_reg(&g_cpu.regs.DE.l); break;
  /* DEC H */
  case 0x25: dec_8bit_reg(&g_cpu.regs.HL.h); break;
  /* DEC L */
  case 0x2D: dec_8bit_reg(&g_cpu.regs.HL.l); break;
  /* DEC (HL) */
  case 0x35: dec_8bit_address(g_cpu.regs.HL.word); break;


  /** ADD A, n to A **/
  /* ADD A, A */
  case 0x87: add_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.AF.h); break;
  /* ADD A, B */
  case 0x80: add_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.BC.h); break;
  /* ADD A, C */
  case 0x81: add_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.BC.l); break;
  /* ADD A, D */
  case 0x82: add_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.DE.h); break;
  /* ADD A, E */
  case 0x83: add_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.DE.l); break;
  /* ADD A, H */
  case 0x84: add_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.HL.h); break;
  /* ADD A, L */
  case 0x85: add_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.HL.l); break;
  /* ADD A, (HL) */
  case 0x86: add_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.HL.word); break;
  /* ADD A, u8 */
  case 0xC6: add_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.PC.word++); break;
    

  /** Add n + Carry flag to A **/
  /* ADC A, A */
  case 0x8F:
      value = g_cpu.regs.AF.h + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
  /* ADC A, B */
  case 0x88:
      value = g_cpu.regs.BC.h + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
  /* ADC A, C */
  case 0x89:
      value = g_cpu.regs.BC.l + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
  /* ADC A, D */
  case 0x8A:
      value = g_cpu.regs.DE.h + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
  /* ADC A, E */
  case 0x8B:
      value = g_cpu.regs.DE.l + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
  /* ADC A, H */
  case 0x8C:
      value = g_cpu.regs.HL.h + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
  /* ADC A, L */
  case 0x8D:
      value = g_cpu.regs.HL.l + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
  /* ADC A, (HL) */
  case 0x8E:
      value = read_address(g_cpu.regs.HL.word) + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
  /* ADC A, u8 */
  case 0xCE:
      value = read_address(g_cpu.regs.PC.word++) + g_cpu.regs.flag_c;
      add_8bit_reg(&g_cpu.regs.AF.h, value); break;
      

  /** SUB A, n from A **/
  /* SUB A, A */
  case 0x97: sub_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.AF.h); break;
  /* SUB A, B */
  case 0x90: sub_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.BC.h); break;
  /* SUB A, C */
  case 0x91: sub_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.BC.l); break;
  /* SUB A, D */
  case 0x92: sub_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.DE.h); break;
  /* SUB A, E */
  case 0x93: sub_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.DE.l); break;
  /* SUB A, H */
  case 0x94: sub_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.HL.h); break;
  /* SUB A, L */
  case 0x95: sub_8bit_reg(&g_cpu.regs.AF.h, g_cpu.regs.HL.l); break;
  /* SUB A, (HL) */
  case 0x96: sub_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.HL.word); break;
  /* SUB A, u8 */
  case 0xD6: sub_8bit_address(&g_cpu.regs.AF.h, g_cpu.regs.PC.word++); break;

  /** Substract n + Carry flag from A **/
  /* SBC A, A */
  case 0x9F: 
      value = g_cpu.regs.AF.h - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
  /* SBC A, B */ 
  case 0x98:
      value = g_cpu.regs.BC.h  - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
  /* SBC A, C */
  case 0x99:
      value = g_cpu.regs.BC.l - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
  /* SBC A, D */
  case 0x9A:
      value = g_cpu.regs.DE.h - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
  /* SBC A, E */
  case 0x9B:
      value = g_cpu.regs.DE.l - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
  /* SBC A, H */
  case 0x9C:
      value = g_cpu.regs.HL.h - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
  /* SBC A, L */
  case 0x9D:
      value = g_cpu.regs.HL.l - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
  /* SBC A, (HL) */
  case 0x9E:
      value = read_address(g_cpu.regs.HL.word) - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
  /* SBC A, u8 */
  case 0xDE:
      value = read_address(g_cpu.regs.PC.word++) - g_cpu.regs.flag_c;
      sub_8bit_reg(&g_cpu.regs.AF.h, value); 
      break;
     
  /** A AND n, result in A **/
  /* AND A, A */
  case 0xA7: and_8bit_reg(g_cpu.regs.AF.h); break;
  /* AND A, B */
  case 0xA0: and_8bit_reg(g_cpu.regs.BC.h); break;
  /* AND A, C */
  case 0xA1: and_8bit_reg(g_cpu.regs.BC.l); break;
  /* AND A, D */
  case 0xA2: and_8bit_reg(g_cpu.regs.DE.h); break;
  /* AND A, E */
  case 0xA3: and_8bit_reg(g_cpu.regs.DE.l); break;
  /* AND A, H */
  case 0xA4: and_8bit_reg(g_cpu.regs.HL.h); break;
  /* AND A, L */
  case 0xA5: and_8bit_reg(g_cpu.regs.HL.l); break;
  /* AND A, (HL) */
  case 0xA6: and_8bit_address(g_cpu.regs.HL.word); break;
  /* AND A, u8 */
  case 0xE6: and_8bit_address(g_cpu.regs.PC.word++); break;

  /** A OR n, result in A **/
  /* OR A, A */
  case 0xB7: or_8bit_reg(g_cpu.regs.AF.h); break;
  /* OR A, B */ 
  case 0xB0: or_8bit_reg(g_cpu.regs.BC.h); break;
  /* OR A, C */
  case 0xB1: or_8bit_reg(g_cpu.regs.BC.l); break;
  /* OR A, D */
  case 0xB2: or_8bit_reg(g_cpu.regs.DE.h); break;
  /* OR A, E */
  case 0xB3: or_8bit_reg(g_cpu.regs.DE.l); break;
  /* OR A, H */
  case 0xB4: or_8bit_reg(g_cpu.regs.HL.h); break;
  /* OR A, L */
  case 0xB5: or_8bit_reg(g_cpu.regs.HL.l); break;
  /* OR A, (HL) */
  case 0xB6: or_8bit_address(g_cpu.regs.HL.word); break;
  /* OR A, u8 */
  case 0xF6: or_8bit_address(g_cpu.regs.PC.word++); break;

  /** A XOR n, result in A **/
  /* XOR A, A */
  case 0xAF: xor_8bit_reg(g_cpu.regs.AF.h); break;
  /* XOR A, B */
  case 0xA8: xor_8bit_reg(g_cpu.regs.BC.h); break;
  /* XOR A, C */
  case 0xA9: xor_8bit_reg(g_cpu.regs.BC.l); break;
  /* XOR A, D */
  case 0xAA: xor_8bit_reg(g_cpu.regs.DE.h); break;
  /* XOR A, E */
  case 0xAB: xor_8bit_reg(g_cpu.regs.DE.l); break;
  /* XOR A, H */
  case 0xAC: xor_8bit_reg(g_cpu.regs.HL.h); break;
  /* XOR A, L */
  case 0xAD: xor_8bit_reg(g_cpu.regs.HL.l); break;
  /* XOR A, (HL) */
  case 0xAE: xor_8bit_address(g_cpu.regs.HL.word); break;
  /* XOR A, u8 */
  case 0xEE: xor_8bit_address(g_cpu.regs.PC.word++); break;

  /** Compare A with n **/
  /* CP A, A */
  case 0xBF: cp_8bit_reg(g_cpu.regs.AF.h); break;
  /* CP A, B */ 
  case 0xB8: cp_8bit_reg(g_cpu.regs.BC.h); break;
  /* CP A, C */
  case 0xB9: cp_8bit_reg(g_cpu.regs.BC.l); break;
  /* CP A, D */
  case 0xBA: cp_8bit_reg(g_cpu.regs.DE.h); break;
  /* CP A, E */
  case 0xBB: cp_8bit_reg(g_cpu.regs.DE.l); break;
  /* CP A, H */
  case 0xBC: cp_8bit_reg(g_cpu.regs.HL.h); break;
  /* CP A, L */
  case 0xBD: cp_8bit_reg(g_cpu.regs.HL.l); break;
  /* CP A, (HL) */
  case 0xBE: cp_8bit_address(g_cpu.regs.HL.word); break;
  /* CP A, u8 */
  case 0xFE: cp_8bit_address(g_cpu.regs.PC.word++); break;

  /******** 16-bit ALU ********/
  /** Increment register nn **/
  /* INC BC */
  case 0x03: inc_16bit(&g_cpu.regs.BC.word); break;
  /* INC DE */
  case 0x13: inc_16bit(&g_cpu.regs.DE.word); break;
  /* INC HL */
  case 0x23: inc_16bit(&g_cpu.regs.HL.word); break;
  /* INC SP */
  case 0x33: inc_16bit(&g_cpu.regs.SP.word); break;


  /** Decrement register nn **/
  /* DEC BC */
  case 0x0B: dec_16bit(&g_cpu.regs.BC.word); break;
  /* DEC DE */
  case 0x1B: dec_16bit(&g_cpu.regs.DE.word); break;
  /* DEC HL */
  case 0x2B: dec_16bit(&g_cpu.regs.HL.word); break;
  /* DEC.SP.word */
  case 0x3B: dec_16bit(&g_cpu.regs.SP.word); break;


  /** Add n to HL **/
  /* ADD HL, BC */
  case 0x09: add_16bit(&g_cpu.regs.HL.word, g_cpu.regs.BC.word); break;
  /* ADD HL, DE */
  case 0x19: add_16bit(&g_cpu.regs.HL.word, g_cpu.regs.DE.word); break;
  /* ADD HL, HL */
  case 0x29: add_16bit(&g_cpu.regs.HL.word, g_cpu.regs.HL.word); break;
  /* ADD HL, SP */
  case 0x39: add_16bit(&g_cpu.regs.HL.word, g_cpu.regs.SP.word); break;


  /** Add n to SP **/
  /* ADD SP, i8 */
  case 0xE8: add_sp(read_address(g_cpu.regs.PC.word++)); break;

    
  /** Put SP + n effective address into HL **/
  /* LD HL, SP + u8 */
  case 0xF8:
    int8_t n = (int8_t)read_address(g_cpu.regs.PC.word++);
    g_cpu.regs.flag_z = 0;
    g_cpu.regs.flag_n = 0;

    uint16_t value = (g_cpu.regs.SP.word + n) & 0xFFFF;

    g_cpu.regs.HL.word = value;

    g_cpu.regs.flag_c = (n > 0xFFFF) ? 1 : 0;

    g_cpu.regs.flag_h = (((g_cpu.regs.SP.word & 0xF) + (n & 0x0F)) > 0x0F) ? 1 : 0;
    break;

  /* LD SP, HL */
  case 0xF9:
    g_cpu.regs.SP.word = g_cpu.regs.HL.word; break;


  /******** 8-bit RSB ********/
  /** Rotate A left. Old bit 7 to Carry Flag **/
  /* RLCA */
  case 0x07: rlca_8bit(); break;
  /** Rotate A right. Old bit 0 to Carry Flag **/
  /* RRCA */
  case 0x0F: rrca_8bit(); break;
  /** Rotate A left through Carry flag **/
  /* RLA */
  case 0x17: rla_8bit(); break;
  /** Rotate A right through Carry flag **/
  /* RRA */
  case 0x1F: rra_8bit(); break;
  /* 16 Bit Opcodes */
  case 0xCB: execute_extended_next(); break;
    

  /******** NO  EXEXUTION ********/
  case 0xD3:
  case 0xDB:
  case 0xDD:
  case 0xE3:
  case 0xE4:
  case 0xEB:
  case 0xEC:
  case 0xED:
  case 0xF4:
  case 0xFC:
  case 0xFD:
    break;
  default:
    printf("Instruction not implemented\n");
    exit(1);
    break;
  }
}
