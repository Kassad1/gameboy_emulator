Q = @
export Q

LD = ld
CC = gcc
CFLAGS = -std=c11 -Wall -I../inc
export LD CC CFLAGS


EMULATOR_OBJS = emulation/emulation_obj.o \
		CPU/cpu_obj.o \
		memory/memory_obj.o \
		other/other_obj.o \
		graphics/graphics_obj.o


all:gameboy_emulator
gameboy_emulator:$(EMULATOR_OBJS)
	$(Q)$(CC) $^ -o $@ -lSDL2 -lSDL2_ttf -std=gnu99

emulation/emulation_obj.o:emulation/*.c
	@make -C emulation/

CPU/cpu_obj.o:CPU/*.c
	@make -C CPU/

memory/memory_obj.o:memory/*.c
	@make -C memory/

other/other_obj.o:other/*.c
	@make -C other/

graphics/graphics_obj.o:graphics/*.c
	@make -C graphics/


clean:
	find . -name *.o | xargs rm -f
	rm -f gameboy_emulator

