#include "graphics.h"
#include "emu.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

SDL_Window *sdlWindow;
SDL_Renderer *sdlRenderer;
SDL_Texture *sdlTexture;
SDL_Surface *screen;

int ui_init(void)
{
  if (SDL_Init(SDL_INIT_VIDEO)) {
    printf("Failed to initialize SDL\n");
    return -1;
  }

  if (TTF_Init()) {
    printf("Failed to initialize TTF");
    return -1;
  }
  
  SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &sdlWindow, &sdlRenderer);
  return 0;
}

void delay(uint32_t ms)
{
  SDL_Delay(ms);
}

void ui_handle_events(void)
{
  SDL_Event e;
  while (SDL_PollEvent(&e) > 0) {
    if (e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_CLOSE) {
      emu_data.kill = true;
    }
  }
}
