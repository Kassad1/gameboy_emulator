

#include <SDL2/SDL.h>
#include <stdlib.h>
#include <stdbool.h>


#define SCREEN_WIDTH 160
#define SCREEN_HEIGHT 144
#define COLORS 3
#define SCALE 2
#define TOTAL_PIXELS_WIDTH 256
#define TOTAL_PIXELS_HEIGHT 256



SDL_Window* g_Win = NULL;
SDL_Surface* g_ScreenSurface = NULL;
SDL_Renderer* g_Rend = NULL;

uint8_t g_framebuffer[SCREEN_WIDTH][SCREEN_HEIGHT][COLORS];




bool init_renender(void){
	bool success = true;
	Uint32 render_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
	g_Rend = SDL_CreateRenderer(gWin, -1, render_flags);
	if(!g_Rend){
		printf("Error creating renderer: %s\n", SDL_GetError());
		success = false;
	}

	return success;
}

bool init_PPU(){
	bool success = true;
	if(!init_SDL())
		success = false;
	if(!init_renender())
		success =  false;
	return success;
}

void calculate_background(void){
	uint8_t SCY = read_address(0xFF42);
	uint8_t SCX = read_address(0xFF43);
	uint8_t color_palette = read_address(0xFF47);


	uint16_t tile_map, tile_data;
	/* LCDCONT MEMORY */
	if((read_address(0xFF40) >> 4) & 1)
		tile_map = 0x8000;
	else
		tile_map = 0x8800;

	if((read_address(0xFF40) >> 3) & 1)
		tile_data = 0x9C00;
	else
		tile_data = 0x9800;

	/* Background color */
	uint8_t color = read_address(0xFF47);

	/* Current line */
	uint8_t current_line = read_address(0xFF44);

	/* Wrap-Y value */
	uint8_t lcd_y = (SCY + current_line) % TOTAL_PIXEL_HEIGHT;

	/* Getting Y height */
	uint16_t tile_y = lcd_y % 8;

	/* Getting the row */
	uint16_t tile_row = (lcd_y / 8) * 32;
	
	/* Inserting value into BG tile set memory */
	for(int i = 0; i < TOTAL_PIXELS_WIDTH; i++){
		/* Wrapping around values */
		uint8_t lcd_x = (SCX + i) % TOTAL_PIXELS_WIDTH;

		uint16_t tile_x = lcd_x % 8;

		uint16_t tile_coloumn = (lcd_x / 8);

		/* Read the value from background map data */
		uint8_t tile_number1 = read_address(tile_data + tile_row + tile_coloumn);


		/* Content of the pixel of row */
		uint8_t tile_location = read_address(tile_map + (tile_number1) * 16);
		
		/* Colors of the pixels */
		uint8_t tile_number2 = read_address((tile_map + (tile_number1) * 16) + 1);


		/* Getting the palette index */
		uint8_t bit_position = 0x80 >> tile_x;
		uint8_t color_id = ( (tile_number2 & bit_position) | (tile_number1 & bit_position) ) >> 6;

		uint8_t reg = 0x00;
		int shift = 0;
		switch(color_id){
			case 0: reg = 0x03; shift = 0; break;
			case 1: reg = 0x0C; shift = 1; break;
			case 2: reg = 0x30; shift = 2; break;
			case 3: reg = 0xC0; shift = 3; break;
			/* Error Handler */
		}

		uint8_t color_background = (color_palette & reg) >> (2 * shift);
		int R, G, B;
		switch(color_background){
			/* White */
			case 0: R = 255; G = 255; B = 255; break;
			/* Light Grey */
			case 1: R = 211; G = 211; B = 211; break;
			/* Dark Grey */
			case 2: R = 169; G = 169; B = 169; break;
			/* Black */
			case 3: R = 0; G = 0; B = 0; break;
			/* Error Handler */
		}
		g_framebuffer[i][current_line][0] = R;
		g_framebuffer[i][current_line][1] = G;
		g_framebuffer[i][current_line][2] = B;
	}
}



void close_emulator(void){
	/*
	SDL_FreeSurface(gScreenSurface);
	g_ScreenSurface = NULL;

	SDL_DestroyRenderer(gRend);
	g_Rend = NULL;

	SDL_DestroyWindow(gWin);
	g_Win = NULL;
	*/
	SDL_Quit();
}
