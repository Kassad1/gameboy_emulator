#include "configure.h"
#include "rom.h"
#include "mem.h"

rom g_rom;

/* Nintendo Logo */
static uint8_t logo[] = {
	0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B,
	0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
	0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
	0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
	0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC,
	0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E	
};


/* ROM Size Table */
static int romsize_table[] = {
  2, 4, 8, 16, 32, 64, 128, 256, 512
};

/* RAM Size Table */
static int ramsize_table[] = {
	0, 1, 1, 4, 16, 4
};

int rom_init(uint8_t *rombytes) {
  /* Legal Cart */
  if (memcmp(&rombytes[0x0104], logo, sizeof(logo)/sizeof(logo[0])))
    return -1;

  g_rom.contr.type = rombytes[CARTRIDGETYPE];
  g_rom.contr.romsize = romsize_table[rombytes[ROMSIZE]] * 16;
  g_rom.contr.ramsize = ramsize_table[rombytes[RAMSIZE]];
  for (int i = 0; i < 15; i++)
    g_rom.title[i] = rombytes[0x0134 + i];
  g_rom.title[15] = '\0';
  
  return 0;
}


int rom_load(const char* file)
{
  int f;
  struct stat st;

  f = open(file, O_RDONLY);

  if (!f) {
    printf("Failed to open cartridge\n");
    return -1;
  }

  if (fstat(f, &st) == -1) {
    printf("Failed to open cartridge\n");
    return -1;
  }

  g_rom.bytes = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, f, 0);

  printf("Cartridge Inserted\n");

  if (rom_init(g_rom.bytes)) {
    printf("Cartridge failed to load\n");
    return -1;
  }

  printf("Cartridge Loaded\n");
  printf("Title      : %s\n", g_rom.title);
  printf("Type       : %2.2x\n", g_rom.contr.type);
  printf("ROM Size   : %d KB\n", g_rom.contr.romsize);
  printf("RAM Size   : %2.2x\n", g_rom.contr.ramsize);

  return 0;
}
