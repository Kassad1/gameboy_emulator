#include "mem.h"
#include "rom.h"
#include "processor.h"
#include "interrupt.h"
#include "io.h"

ram g_ram;


static uint8_t rom_read(uint16_t address) {
  return g_rom.bytes[address];
}

static void rom_write(uint16_t address, uint8_t value) {
  return;
}

static uint8_t wram_read(uint16_t address) {
  if (address >= 0x2000) {
    printf("Out of Range WRAM: %04X\n", address);
  }
  return g_ram.w_ram[address];
}

static void wram_write(uint16_t address, uint8_t read) {
  if (address >= 0x2000) {
    printf("Out of Range WRAM: %04X\n", address);
  };
  g_ram.w_ram[address] = read;
}

static uint8_t hram_read(uint8_t address) {
  if (address >= 0x80) {
    printf("Out of Range HRAM: %04X\n", address);
  }
  return g_ram.h_ram[address];
}

static void hram_write(uint8_t address, uint8_t read) {
  if (address >= 0x80) {
    printf("Out of Range WRAM: %04X\n", address);
  }
  g_ram.h_ram[address] = read;
}

uint8_t read_address(uint16_t address) {
  if (address >= 0x0000 && address <= 0x7FFF) { /* ROM */
      return rom_read(address);
  } else if (address >= 0x8000 && address <= 0x9FFF) { /* VRAM */
    printf("Not implemented: Address 0x8000 to 0x9FFF\n");
    //exit(-1);
  } else if (address >= 0xA000 && address <= 0xBFFF) { /* External RAM */
       return rom_read(address);
  } else if (address >= 0xC000 && address <= 0xCFFF) { /* Work RAM */
       return wram_read(address - 0xC000);
  } else if (address >= 0xD000 && address <= 0xDFFF) { /* Switchable Work RAM */
       return wram_read(address - 0xC000);
  } else if (address >= 0xE000 && address <= 0xFDFF) { /* Echo RAM */
       return 0;
  } else if (address >= 0xFE00 && address <= 0xFE9F) { /* Object attribute */
    printf("Not implemented: Address 0xFE00 to 0xFE9F\n");
    //exit(-1);
  } else if (address >= 0xFEA0 && address <= 0xFEFF) { /* Not usable */
       return 0;
  } else if (address >= 0xFF00 && address <= 0xFF7F) { /* I/O Register */
    return io_read(address);
  } else if (address >= 0xFF80 && address <= 0xFFFE) { /* High Ram */
    return hram_read(address - 0xFF80);
  } else if (address == 0xFFFF) { /* Interrupt Enable register */
    return (uint8_t) cpu_interrupt.enable;
  }

  return 0x00;
}


void write_address(uint16_t address, uint8_t data) {
  if (address >= 0x0000 && address <= 0x7FFF) { /* ROM */
      return rom_write(address, data);
  } else if (address >= 0x8000 && address <= 0x9FFF) { /* VRAM */
    printf("Not implemented: Address 0x8000 to 0x9FFF\n");
    //exit(-1);
  } else if (address >= 0xA000 && address <= 0xBFFF) { /* External RAM */
      return rom_write(address, data);
  } else if (address >= 0xC000 && address <= 0xCFFF) { /* Work RAM */
      return wram_write(address - 0xC000, data);
  } else if (address >= 0xD000 && address <= 0xDFFF) { /* Switchable Work RAM */
      return wram_write(address - 0xC000, data);
  } else if (address >= 0xE000 && address <= 0xFDFF) { /* Echo RAM */
      return;
  } else if (address >= 0xFE00 && address <= 0xFE9F) { /* Object attribute */
    printf("Not implemented: Address 0xFE00 to 0xFE9F\n");
    //exit(-1);
  } else if (address >= 0xFEA0 && address <= 0xFEFF) { /* Not usable */
      return;
  } else if (address >= 0xFF00 && address <= 0xFF7F) { /* I/O Register */
    io_write(address, data);
  } else if (address >= 0xFF80 && address <= 0xFFFE) { /* High Ram */
    hram_write(address - 0xFF80, data);
  } else if (address == 0xFFFF) { /* Interrupt Enable register */
    cpu_interrupt.enable = (bool) data;
  }
}
