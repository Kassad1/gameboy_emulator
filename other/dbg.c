#include "mem.h"
#include "dbg.h"
/******* Taken from the guide *******/
static char dbg_msg[1024] = {0};
static int msg_size = 0;


void dbg_update(void)
{
  if(read_address(0xFF02) == 0x81) {
    char c = read_address(0xFF01);

    dbg_msg[msg_size++] = c;

    write_address(0xFF02, 0);
  }
}

void dbg_print(void)
{
  if(dbg_msg[0]) {
    printf("DBG: %s\n", dbg_msg);
  }
}
