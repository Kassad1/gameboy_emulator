#include "interrupt.h"
#include "processor.h"

interrupt cpu_interrupt;

void interrupt_init(void)
{
  cpu_interrupt.master = false;
  cpu_interrupt.enable = false;
  cpu_interrupt.reg = 0x00;
  cpu_interrupt.flags = 0x00;
}

void interrupt_service(uint16_t address)
{
  stack_push(g_cpu.regs.PC.word);
  g_cpu.regs.PC.word = address;
}

int interrupt_handler(uint16_t address, uint8_t type)
{
  if((cpu_interrupt.flags & type) && (cpu_interrupt.reg & type)) {
    interrupt_service(address);
    cpu_interrupt.flags &= ~type;
    g_cpu.halt = false;
    cpu_interrupt.master = true;
    return 0;
  }

  return -1;
}

void interrupt_table(void)
{
  if(interrupt_handler(0x40, INTERRUPT_VBLANK)){
    
  }else if (interrupt_handler(0x48, INTERRUPT_LCD_STAT)){

  }else if (interrupt_handler(0x50, INTERRUPT_TIMER)){

  }else if (interrupt_handler(0x58, INTERRUPT_SERIAL)){

  }else if (interrupt_handler(0x60, INTERRUPT_JOYPAD)){

  }
}



int interrupt_step(void)
{
  if(cpu_interrupt.master) {
    interrupt_table();
    cpu_interrupt.enable = false;
  }

  if(cpu_interrupt.enable) {
    cpu_interrupt.master = true;
  }
  
  return 0;
}
