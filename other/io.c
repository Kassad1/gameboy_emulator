#include "io.h"

static char serial_data[2];

uint8_t io_read(uint16_t address)
{
  if (address == 0xFF01) {
    return serial_data[0];
  }

  if (address == 0xFF02) {
    return serial_data[1];
  }
  return 0;
}

void io_write(uint16_t address, uint8_t value)
{
  if (address == 0xFF01) {
    serial_data[0] = value;
  }

  if (address == 0xFF02) {
    serial_data[1] = value;
  }
}
