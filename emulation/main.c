#include "configure.h"
#include "emu.h"

int main(int argc, char **argv)
{
  if (argc < 2) {
    printf("No ROM included\n");
    return -1;
  }

  /* Running the emulator */
  return run(argv[1]);
}
