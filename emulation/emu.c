#include <pthread.h>
#include <unistd.h>

#include "configure.h"
#include "graphics.h"
#include "emu.h"
#include "processor.h"
#include "mem.h"
#include "rom.h"
#include "interrupt.h"


emu_info emu_data;
/*
void *cpu_run(void *p)
{
  if (processor_init()) {
    printf("Failed to initialize processor");
    exit(-1);
  }

  interrupt_init();
  
  emu_data.pause = false;
  emu_data.run = true;
  emu_data.kill = false;
  emu_data.ticks = 0;


  while (emu_data.run) {

    if(cpu_step()) {
      printf("Processor paused\n");
      exit(-1);
    }

    if(interrupt_step()) {
      printf("Inturrept failed\n");
      return -1;
    }

    emu_data.ticks++;
  }
  return 0;
}
*/
int run(char *cart)
{
  if (rom_load(cart)) {
    printf("Unable to load ROM: %s\n", cart);
    return -1;
  }

  printf("ROM loaded: %s\n", cart);

  /*
  if (ui_init()) {
    printf("Failed to create UI");
    exit(-1);
  }
  */
  /*
  pthread_t t1;

  if (pthread_create(&t1, NULL, cpu_run, NULL)) {
    fprintf(stderr, "Failed to create thread for cpu\n");
    return -1;
  }
  */

  /*
  while (!emu_data.kill) {
    usleep(1000);
    ui_handle_events();
  }
  */

  if (processor_init()) {
    printf("Failed to initialize processor");
    exit(-1);
  }

  interrupt_init();
  
  emu_data.pause = false;
  emu_data.run = true;
  emu_data.kill = false;
  emu_data.ticks = 0;


  while (emu_data.run) {

    if(cpu_step()) {
      printf("Processor paused\n");
      exit(-1);
    }

    /*
    if(interrupt_step()) {
      printf("Inturrept failed\n");
      return -1;
    }
    */

    emu_data.ticks++;
  }
  return 0;
}
