#include "processor.h"

//Work on fetching next


void execute_next(void){
	uint8_t opcode = g_cpu.PC++;
	switch(opcode){
		/******** BRANCH ********/
		/* NOP */
		case 0x00: g_cpu.cycle += 1; break;
		/* STOP */
		case 0x10: stop(); break;
		/* DAA */
		case 0x27: DAA(); break;	
		/* HALT */
		case 0x76: halt(); break;
		/* PREFIX CB */
		case 0xCB: break; /* Maybe come to this later if its nesscary */
		/* DI */
		case 0xF3: interrupt_state(0); break;
		/* EI */
		case 0xFB: interrupt_state(1); break;	
		/******** MISC ********/
		/** JUMPING to an address with adding n **/
		/* JR i8 */
		case 0x18: jump_immediate(true, false, false); break;
		/* JR NZ, i8 */
		case 0x20: jump_immediate(false, g_cpu.flag_z, false); break;
		/* JR Z, i8 */
		case 0x28: jump_immediate(false, g_cpu.flag_z, true); break;
		/* JR NC, i8 */
		case 0x30: jump_immediate(false, g_cpu.flag_c, false); break;
		/* JR C, i8 */
		case 0x38: jump_immediate(false, g_cpu.flag_c, true); break;
		/** Return if true **/
		/* RET */
		case 0xC9: flag_return(true, false, false); break;
		/* RET NZ */
		case 0xC0: flag_return(false, g_cpu.flag_z, false); break;
		/* RET Z */
		case 0xC8: flag_return(false, g_cpu.flag_z, true); break;
		/* RET NC */
		case 0xD0: flag_return(false, g_cpu.flag_c, false); break;
		/* RET C */
		case 0xD8: flag_return(false, g_cpu.flag_c, true); break;
		/** Jump to address nn **/
		/* JP u16 */
		case 0xC3: jump_address(true, false, false); break; 
		/* JP NZ, u16 */
		case 0xC2: jump_address(false, g_cpu.flag_z, false); break;
		/* JP Z, u16 */
		case 0xCA: jump_address(false, g_cpu.flag_z, true); break;
		/* JP NC, u16 */
		case 0xD2: jump_address(false, g_cpu.flag_c, false); break;
		/* JP C, u16 */
		case 0xDA: jump_address(false, g_cpu.flag_c, true); break;
		/** Calls **/
		/* CALL u16 */
		case 0xCD: call_address(true, false, false); break;
		/* CALL NZ, u16 */
		case 0xC4: call_address(false, g_cpu.flag_z, false); break;
		/* CALL Z, u16 */
		case 0xCC: call_address(false, g_cpu.flag_z, true); break;
		/* CALL NC, u16 */
		case 0xD4: call_address(false, g_cpu.flag_c, false); break;
		/* CALL C, u16 */
		case 0xDC: call_address(false, g_cpu.flag_c, true); break;
		/** Push address onto stack **/
		/* RST 00h */
		case 0xC7: cpu_reset(0x00); break;
		/* RST 08h */
		case 0xCF: cpu_reset(0x08); break;
		/* RST 10h */
		case 0xD7: cpu_reset(0x10); break;
		/* RST 18h */
		case 0xDF: cpu_reset(0x18); break;
		/* RST 20h */
		case 0xE7: cpu_reset(0x20); break;
		/* RST 28h */
		case 0xEF: cpu_reset(0x28); break;
		/* RST 30h */
		case 0xF7: cpu_reset(0x30); break;
		/* RST 38h */
		case 0xFF: cpu_reset(0x38); break;
		
			   
		/** RANDOM **/
		/* SCF */
		case 0x37:
		break;
		/* CPL */
		case 0x2F:
		break;
		/* CCF */
		case 0x3F:
		break;
		/* Pop two bytes from stack & jump to address, enable interrupts */
		case 0xD9: 
		break;


		/******** 8-bit Load/Store/Move ********/
		/** Put value nn into n **/
		/* LD B, u8 */
		case 0x06: write_regs_hi(&g_cpu.regs.BC, read_address(g_cpu.regs.PC++)); break;
		/* LD C, u8 */
		case 0x0E: write_regs_lo(&g_cpu.regs.BC, read_address(g_cpu.regs.PC++)); break;
		/* LD D, u8 */
		case 0x16: write_regs_hi(&g_cpu.regs.DE, read_address(g_cpu.regs.PC++)); break;
		/* LD E, u8 */
		case 0x1E: write_regs_lo(&g_cpu.regs.DE, read_address(g_cpu.regs.PC++)); break;
		/* LD H, u8 */
		case 0x26: write_regs_hi(&g_cpu.regs.HL, read_address(g_cpu.regs.PC++)); break;
		/* LD L, u8 */
		case 0x2E: write_regs_hi(&g_cpu.regs.HL, read_address(g_cpu.regs.PC++)); break;


		/** Put value r2 into r1 **/
		/* LD A, A */
		case 0x7F: load_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.AF), 0); break;
		/* LD A, B */
		case 0x78: load_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.BC), 0); break;
		/* LD A, C */
		case 0x79: load_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.BC), 0); break;
		/* LD A, D */
		case 0x7A: load_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.DE), 0); break;
		/* LD A, E */
		case 0x7B: load_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.DE), 0); break;
		/* LD A, H */
		case 0x7C: load_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.HL), 0); break;
		/* LD A, L */
		case 0x7D: load_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.HL), 0); break;
		/* LD A, (HL) */
		case 0x7E: load_8bit(&g_cpu.regs.AF, read_regs(g_cpu.regs.HL), 0); break;


		/* LD B, B */
		case 0x40: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.BC), 0); break;
		/* LD B, C */
		case 0x41: load_8bit(&g_cpu.regs.BC, read_regs_lo(g_cpu.regs.BC), 0); break;
		/* LD B, D */
		case 0x42: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.DE), 0); break;
		/* LD B, E */
		case 0x43: load_8bit(&g_cpu.regs.BC, read_regs_lo(g_cpu.regs.DE), 0); break;
		/* LD B, H */
		case 0x44: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.HL), 0); break;
		/* LD B, L */
		case 0x45: load_8bit(&g_cpu.regs.BC, read_regs_lo(g_cpu.regs.HL), 0); break;
		/* LD B, (HL) */
		case 0x46: load_8bit(&g_cpu.regs.BC, read_regs(g_cpu.regs.HL), 0); break;


		/* LD C, B */
		case 0x48: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.BC), 1); break;
		/* LD C, C */
		case 0x49: load_8bit(&g_cpu.regs.BC, read_regs_lo(g_cpu.regs.BC), 1); break;
		/* LD C, D */
		case 0x4A: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.DE), 1); break;
		/* LD C, E */
		case 0x4B: load_8bit(&g_cpu.regs.BC, read_regs_lo(g_cpu.regs.DE), 1); break;
		/* LD C, H */
		case 0x4C: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.HL), 1); break;
		/* LD C, L */
		case 0x4D: load_8bit(&g_cpu.regs.BC, read_regs_lo(g_cpu.regs.HL), 1); break; 
		/* LD C, (HL) */
		case 0x4E: load_8bit(&g_cpu.regs.BC, read_regs(g_cpu.regs.HL), 1); break;


		/* LD D, B */
		case 0x50: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.BC), 0); break;
		/* LD D, C */
		case 0x51: load_8bit(&g_cpu.regs.DE, read_regs_lo(g_cpu.regs.BC), 0); break;
		/* LD D, D */
		case 0x52: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.DE), 0); break;
		/* LD D, E */
		case 0x53: load_8bit(&g_cpu.regs.DE, read_regs_lo(g_cpu.regs.DE), 0); break;
		/* LD D, H */
		case 0x54: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.HL), 0); break;
		/* LD D, L */
		case 0x55: load_8bit(&g_cpu.regs.DE, read_regs_lo(g_cpu.regs.HL), 0); break;
		/* LD D, (HL) */
		case 0x56: load_8bit(&g_cpu.regs.DE, read_regs(g_cpu.regs.HL), 0); break;


		/* LD E, B */
		case 0x58: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.BC), 1); break;
		/* LD E, C */
		case 0x59: load_8bit(&g_cpu.regs.DE, read_regs_lo(g_cpu.regs.BC), 1); break;
		/* LD E, D */
		case 0x5A: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.DE), 1); break;
		/* LD E, E */
		case 0x5B: load_8bit(&g_cpu.regs.DE, read_regs_lo(g_cpu.regs.DE), 1); break;
		/* LD E, H */
		case 0x5C: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.HL), 1); break;
		/* LD E, L */
		case 0x5D: load_8bit(&g_cpu.regs.DE, read_regs_lo(g_cpu.regs.HL), 1); break;
		/* LD E, (HL) */
		case 0x5E: load_8bit(&g_cpu.regs.DE, read_regs(g_cpu.regs.HL), 1); break;


		/* LD H, B */
		case 0x60: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.BC), 0); break;
		/* LD H, C */
		case 0x61: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.BC), 0); break;
		/* LD H, D */
		case 0x62: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.DE), 0); break;
		/* LD H, E */
		case 0x63: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.DE), 0); break;
		/* LD H, H */
		case 0x64: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.HL), 0); break;
		/* LD H, L */
		case 0x65: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.HL), 0); break;
		/* LD H, (HL) */
		case 0x66: load_8bit(&g_cpu.regs.HL, read_regs(g_cpu.regs.HL), 0); break;


		/* LD L, B */
		case 0x68: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.BC), 1); break;
		/* LD L, C */
		case 0x69: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.BC), 1); break;
		/* LD L, D */
		case 0x6A: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.DE), 1); break;
		/* LD L, E */
		case 0x6B: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.DE), 1); break;
		/* LD L, H */
		case 0x6C: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.HL), 1); break;
		/* LD L, L */
		case 0x6D: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.HL), 1); break;
		/* LD L, (HL) */
		case 0x6E: load_8bit(&g_cpu.regs.HL, read_regs(g_cpu.regs.HL), 1); break;


		/* LD (HL), B */
		case 0x70: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.BC), 2); break;
		/* LD (HL), C */
		case 0x71: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.BC), 2); break;
		/* LD (HL), D */
		case 0x72: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.DE), 2); break;
		/* LD (HL), E */
		case 0x73: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.DE), 2); break;
		/* LD (HL), H */
		case 0x74: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.HL), 2); break;
		/* LD (HL), L */
		case 0x75: load_8bit(&g_cpu.regs.HL, read_regs_lo(g_cpu.regs.HL), 2); break;
		/* LD (HL), u8 */
		case 0x36: load_8bit(&g_cpu.regs.HL, g_cpu.regs.PC++, 2); break;


		/** Put value n into A **/
		/* LD A, (BC) */
		case 0x0A: load_8bit(&g_cpu.regs.AF, read_regs(g_cpu.regs.BC), 0); break;
		/* LD A, (DE) */
		case 0x1A: load_8bit(&g_cpu.regs.AF, read_regs(g_cpu.regs.DE), 0); break;
		/* LD A, (HL) */
		case 0x7E: load_8bit(&g_cpu.regs.AF, read_regs(g_cpu.regs.HL), 0); break;
		/* LD A, (nn) */
		case 0xFA: 
			   uint16_t temp = (0x00FF & g_cpu.regs.PC++) | (0xFF00 & g_cpu.regs.PC++);
			   load_8bit(&g_cpu.regs.AF, read_address(temp), 0); break;
		/* LD A, u8 */
		case 0x3E: load_8bit(&g_cpu.regs.AF, g_cpu.regs.PC++, 0); break;


		/** Put value A into n **/
		/* LD B, A */
		case 0x47: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.AF), 0); break;
		/* LD C, A */
		case 0x4F: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.AF), 1); break;
		/* LD D, A */
		case 0x57: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.AF), 0); break;
		/* LD E, A */
		case 0x5F: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.AF), 1); break;
		/* LD H, A */
		case 0x67: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.AF), 0); break;
		/* LD L, A */
		case 0x6F: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.AF), 1); break;
		/* LD (BC), A */
		case 0x02: load_8bit(&g_cpu.regs.BC, read_regs_hi(g_cpu.regs.AF), 2); break;
		/* LD (DE), A */
		case 0x12: load_8bit(&g_cpu.regs.DE, read_regs_hi(g_cpu.regs.AF), 2); break;
		/* LD (HL), A */ 
		case 0x77: load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.AF), 2); break; 
		/* LD (nn), A */ 
		case 0xEA: 
			   uint16_t temp = (0x00FF & g_cpu.regs.PC++) | (0xFF00 & g_cpu.regs.PC++); 
			   load_8bit(&temp, read_regs_hi(g_cpu.regs.AF), 2); break;


		/** Put value at address $FF00 + C into A **/
		/* LD A, (C) */
		case 0xF2:
			   uint16_t temp = 0xFF00 | g_cpu.regs.PC++;
			   load_8bit(&g_cpu.regs.AF, temp, 0); break;
		/** Put A into address $FF00 + C **/
		/* LD (C), A */
		case 0xE2:
			   uint16_t temp = 0xFF00 | g_cpu.regs.PC++;
			   load_8bit(&temp, read_regs_hi(g_cpu.regs.AF), 2); break;
		/** Put value at address HL into A. Dec HL **/
		/* LD A, (HLD) */
		case 0x3A: load_8bit(&g_cpu.regs.AF, read_regs(g_cpu.regs.HL--), 0); break;
		/** Put A into memory address HL. Dec HL **/
		/* LD (HLD), A */
		case 0x32: 
			   load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.AF), 2);
			   g_cpu.regs.HL--; break;
		/** Put value at address HL into A. Inc HL **/
		/* LD A, (HLI) */
		case 0x2A: load_8bit(&g_cpu.regs.AF, read_regs(g_cpu.regs.HL++), 0); break;
		/* LD (HLI), A */
		case 0x22:
			   load_8bit(&g_cpu.regs.HL, read_regs_hi(g_cpu.regs.AF), 2);
			   g_cpu.regs.HL++; break;
		/* LD ($FF00 + n), A */
		case 0xE0:
			   uint16_t temp = 0xFF00 | g_cpu.regs.PC++;
			   load_8bit(&temp, read_regs_hi(g_cpu.regs.AF), 2); break;
		/** Put memory address $FF00 + n into A **/
		/* LD A, ($FF00 + n) */
		case 0xF0: load_8bit(&g_cpu.regs.AF, 0xFF00 | g_cpu.regs.PC++, 0); break;


		/******** 16-bit Load/Store/Move ********/
		/** Loads value u16 into n **/
		/* LD BC, u16 */
		case 0x01: load_16bit(&g_cpu.regs.BC, g_cpu.regs.PC++); break;
		/* LD DE, u16 */
		case 0x11: load_16bit(&g_cpu.regs.DE, g_cpu.regs.PC++); break;
		/* LD HL, u16 */
		case 0x21: load_16bit(&g_cpu.regs.HL, g_cpu.regs.PC++); break;
		/* LD SP, u16 */
		case 0x31: load_16bit(&g_cpu.regs.SP, g_cpu.regs.PC++); break;
		/** Put SP value at address n **/
		/* LD (u16), SP */
		case 0x08: load_16bit(&g_cpu.regs.PC++, g_cpu.regs.SP); break;
		
		
		/** POP two bytes off stack into nn **/
		/* POP AF */
		case 0xF1: g_cpu.regs.AF = stack_pop(); break;
		/* POP BC */
		case 0xC1: g_cpu.regs.BC = stack_pop(); break; 
		/* POP DE */
		case 0xD1: g_cpu.regs.DE = stack_pop(); break;
		/* POP HL */
		case 0xE1: g_cpu.regs.HL = stack_pop(); break;


		/** PUSH two bytes into stack from nn **/
		/* PUSH AF */
		case 0xF5: stack_push(g_cpu.regs.AF); break;
		/* PUSH BC */
		case 0xC5: stack_push(g_cpu.regs.BC); break;
		/* PUSH DE */
		case 0xD5: stack_push(g_cpu.regs.DE); break; 
		/* PUSH HL */
		case 0xE5: stack_push(g_cpu.regs.HL); break;


		/** HL into SP **/
		/* LD SP, HL */
		case 0xF9: load_16bit(&g_cpu.regs.SP, g_cpu.regs.HL); break;


		/******** 8-bit ALU ********/
		/** Increment register n **/
		/* INC A */
		case 0x3C: inc_8bit(&g_cpu.regs.AF, 0x01, 1); break;
		/* INC B */
		case 0x04: inc_8bit(&g_cpu.regs.BC, 0x01, 1); break;
		/* INC C */
		case 0x0C: inc_8bit(&g_cpu.regs.BC, 0x01, 0); break;
		/* INC D */
		case 0x14: inc_8bit(&g_cpu.regs.DE, 0x01, 1); break;
		/* INC E */
		case 0x1C: inc_8bit(&g_cpu.regs.DE, 0x01, 0); break;
		/* INC H */
		case 0x24: inc_8bit(&g_cpu.regs.HL, 0x01, 1); break;
		/* INC L */
		case 0x2C: inc_8bit(&g_cpu.regs.HL, 0x01, 0); break;
		/* INC (HL) */
		case 0x34: inc_8bit(&g_cpu.regs.HL, 0x01, 2); break;

	
		/** Decrement register n **/
		/* DEC A */
		case 0x3D: dec_8bit(&g_cpu.regs.AF, 0x01, 1); break;
		/* DEC B */
		case 0x05: dec_8bit(&g_cpu.regs.BC, 0x01, 1); break;
		/* DEC C */
		case 0x0D: dec_8bit(&g_cpu.regs.BC, 0x01, 0); break;
		/* DEC D */
		case 0x15: dec_8bit(&g_cpu.regs.DE, 0x01, 1); break;
		/* DEC E */
		case 0x1D: dec_8bit(&g_cpu.regs.DE, 0x01, 0); break;
		/* DEC H */
		case 0x25: dec_8bit(&g_cpu.regs.HL, 0x01, 1); break;
		/* DEC L */
		case 0x2D: dec_8bit(&g_cpu.regs.HL, 0x01, 0); break;
		/* DEC (HL) */
		case 0x35: dec_8bit(&g_cpu.regs.HL, 0x01, 2); break;
		
			   
		/** ADD A, n to A **/
		/* ADD A, A */
		case 0x87: add_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.AF)); break;
		/* ADD A, B */
		case 0x80: add_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.BC)); break;
		/* ADD A, C */
		case 0x81: add_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.BC)); break;
		/* ADD A, D */
		case 0x82: add_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.DE)); break;
		/* ADD A, E */
		case 0x83: add_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.DE)); break;
		/* ADD A, H */
		case 0x84: add_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.HL)); break;
		/* ADD A, L */
		case 0x85: add_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.HL)); break;
		/* ADD A, (HL) */
		case 0x86: add_8bit(&g_cpu.regs.AF, read_regs(g_cpu.regs.HL)); break;
		/* ADD A, u8 */
		case 0xC6: add_8bit(&g_cpu.regs.AF, g_cpu.cycle.PC++); break;


		/** Add n + Carry flag to A **/
		/* ADC A, A */
		case 0x8F: 
			   uint8_t temp = read_regs_hi(g_cpu.regs.AF) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		/* ADC A, B */
		case 0x88:
			   uint8_t temp = read_regs_hi(g_cpu.regs.BC) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		/* ADC A, C */
		case 0x89:
			   uint8_t temp = read_regs_lo(g_cpu.regs.BC) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		/* ADC A, D */
		case 0x8A:
			   uint8_t temp = read_regs_hi(g_cpu.regs.DE) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		/* ADC A, E */
		case 0x8B:
			   uint8_t temp = read_regs_lo(g_cpu.regs.DE) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		/* ADC A, H */
		case 0x8C:
			   uint8_t temp = read_regs_hi(g_cpu.regs.HL) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		/* ADC A, L */
		case 0x8D:
			   uint8_t temp = read_regs_lo(g_cpu.regs.HL) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		/* ADC A, (HL) */
		case 0x8E:
			   uint8_t temp = read_regs(g_cpu.regs.HL) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		/* ADC A, u8 */
		case 0xCE:
			   uint8_t temp = (g_cpu.regs.PC++) + g_cpu.regs.flag_c;
			   add_8bit(&g_cpu.regs.AF, temp); break;
		
			   
		/** SUB A, n from A **/
		/* SUB A, A */
		case 0x97: sub_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.AF)); break;
		/* SUB A, B */
		case 0x90: sub_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.BC)); break;
		/* SUB A, C */
		case 0x91: sub_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.BC)); break;
		/* SUB A, D */
		case 0x92: sub_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.DE)); break;
		/* SUB A, E */
		case 0x93: sub_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.DE)); break;
		/* SUB A, H */
		case 0x94: sub_8bit(&g_cpu.regs.AF, read_regs_hi(g_cpu.regs.HL)); break;
		/* SUB A, L */
		case 0x95: sub_8bit(&g_cpu.regs.AF, read_regs_lo(g_cpu.regs.HL)); break;
		/* SUB A, (HL) */
		case 0x96: sub_8bit(&g_cpu.regs.AF, read_regs(g_cpu.regs.HL)); break;
		/* SUB A, u8 */
		case 0xD6: sub_8bit(&g_cpu.regs.AF, g_cpu.regs.PC++); break;


		/** Substract n + Carry flag from A **/
		/* SBC A, A */
		case 0x9F:
			   uint8_t temp = read_regs_hi(g_cpu.regs.AF) - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;
		/* SBC A, B */ 
		case 0x98:
			   uint8_t temp = read_regs_hi(g_cpu.regs.BC) - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;
		/* SBC A, C */
		case 0x99:
			   uint8_t temp = read_regs_lo(g_cpu.regs.BC) - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;
		/* SBC A, D */
		case 0x9A:
			   uint8_t temp = read_regs_hi(g_cpu.regs.DE) - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;
		/* SBC A, E */
		case 0x9B:
			   uint8_t temp = read_regs_lo(g_cpu.regs.DE) - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;
		/* SBC A, H */
		case 0x9C:
			   uint8_t temp = read_regs_hi(g_cpu.regs.HL) - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;
		/* SBC A, L */
		case 0x9D:
			   uint8_t temp = read_regs_lo(g_cpu.regs.HL) - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;
		/* SBC A, (HL) */
		case 0x9E:
			   uint8_t temp = read_address(g_cpu.regs.HL) - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;
		/* SBC A, u8 */
		case 0xDE:
			   uint8_t temp = g_cpu.regs. - g_cpu.regs.flag_c;
			   sub_8bit(&g_cpu.regs.AF, temp); break;


		/** A AND n, result in A **/
		/* AND A, A */
		case 0xA7: and_8bit(read_regs_hi(g_cpu.regs.AF)); break;
		/* AND A, B */
		case 0xA0: and_8bit(read_regs_hi(g_cpu.regs.BC)); break;
		/* AND A, C */
		case 0xA1: and_8bit(read_regs_lo(g_cpu.regs.BC)); break;
		/* AND A, D */
		case 0xA2: and_8bit(read_regs_hi(g_cpu.regs.DE)); break;
		/* AND A, E */
		case 0xA3: and_8bit(read_regs_lo(g_cpu.regs.DE)); break;
		/* AND A, H */
		case 0xA4: and_8bit(read_regs_hi(g_cpu.regs.HL)); break;
		/* AND A, L */
		case 0xA5: and_8bit(read_regs_lo(g_cpu.regs.HL)); break;
		/* AND A, (HL) */
		case 0xA6: and_8bit(read_address(g_cpu.regs.HL)); break;
		/* AND A, u8 */
		case 0xE6: and_8bit(read_address(g_cpu.regs.PC++)); break;


		/** A OR n, result in A **/
		/* OR A, A */
		case 0xB7: or_8bit(read_regs_hi(g_cpu.regs.AF)); break;
		/* OR A, B */ 
		case 0xB0: or_8bit(read_regs_hi(g_cpu.regs.BC)); break;
		/* OR A, C */
		case 0xB1: or_8bit(read_regs_lo(g_cpu.regs.BC)); break;
		/* OR A, D */
		case 0xB2: or_8bit(read_regs_hi(g_cpu.regs.DE)); break;
		/* OR A, E */
		case 0xB3: or_8bit(read_regs_lo(g_cpu.regs.DE)); break;
		/* OR A, H */
		case 0xB4: or_8bit(read_regs_hi(g_cpu.regs.HL)); break;
		/* OR A, L */
		case 0xB5: or_8bit(read_regs_hi(g_cpu.regs.HL)); break;
		/* OR A, (HL) */
		case 0xB6: or_8bit(read_address(g_cpu.regs.HL)); break;
		/* OR A, u8 */
		case 0xF6: or_8bit(read_regs_hi(g_cpu.regs.PC++)); break;


		/** A XOR n, result in A **/
		/* XOR A, A */
		case 0xAF: xor_8bit(read_regs_hi(g_cpu.regs.AF)); break;
		/* XOR A, B */
		case 0xA8: xor_8bit(read_regs_hi(g_cpu.regs.BC)); break;
		/* XOR A, C */
		case 0xA9: xor_8bit(read_regs_lo(g_cpu.regs.BC)); break;
		/* XOR A, D */
		case 0xAA: xor_8bit(read_regs_hi(g_cpu.regs.DE)); break;
		/* XOR A, E */
		case 0xAB: xor_8bit(read_regs_lo(g_cpu.regs.DE)); break;
		/* XOR A, H */
		case 0xAC: xor_8bit(read_regs_hi(g_cpu.regs.HL)); break;
		/* XOR A, L */
		case 0xAD: xor_8bit(read_regs_lo(g_cpu.regs.HL)); break;
		/* XOR A, (HL) */
		case 0xAE: xor_8bit(read_address(g_cpu.regs.HL)); break;
		/* XOR A, u8 */
		case 0xEE: xor_8bit(read_address(g_cpu.regs.PC++)); break;


		/** Compare A with n **/
		/* CP A, A */
		case 0xBF: cp_8bit(read_regs_hi(g_cpu.regs.AF)); break;
		/* CP A, B */ 
		case 0xB8: cp_8bit(read_regs_hi(g_cpu.regs.BC)); break;
		/* CP A, C */
		case 0xB9: cp_8bit(read_regs_lo(g_cpu.regs.BC)); break;
		/* CP A, D */
		case 0xBA: cp_8bit(read_regs_hi(g_cpu.regs.DE)); break;
		/* CP A, E */
		case 0xBB: cp_8bit(read_regs_lo(g_cpu.regs.DE)); break;
		/* CP A, H */
		case 0xBC: cp_8bit(read_regs_hi(g_cpu.regs.HL)); break;
		/* CP A, L */
		case 0xBD: cp_8bit(read_regs_lo(g_cpu.regs.HL)); break;
		/* CP A, (HL) */
		case 0xBE: cp_8bit(read_address(g_cpu.regs.HL)); break;
		/* CP A, u8 */
		case 0xFE: cp_8bit(read_address(g_cpu.reg.PC++)); break;


		/******** 16-bit ALU ********/
		/** Increment register nn **/
		/* INC BC */
		case 0x03: inc_16bit(&g_cpu.regs.BC); break;
		/* INC DE */
		case 0x13: inc_16bit(&g_cpu.regs.DE); break;
		/* INC HL */
		case 0x23: inc_16bit(&g_cpu.regs.HL); break;
		/* INC SP */
		case 0x33: inc_16bit(&g_cpu.regs.SP); break;


		/** Decrement register nn **/
		/* DEC BC */
		case 0x0B: dec_16bit(&g_cpu.regs.BC); break;
		/* DEC DE */
		case 0x1B: dec_16bit(&g_cpu.regs.DE); break;
		/* DEC HL */
		case 0x2B: dec_16bit(&g_cpu.regs.HL); break;
		/* DEC SP */
		case 0x3B: dec_16bit(&g_cpu.regs.SP); break;


		/** Add n to HL **/
		/* ADD HL, BC */
		case 0x09: add_16bit(&g_cpu.regs.HL, g_cpu.regs.BC);
		/* ADD HL, DE */
		case 0x19: add_16bit(&g_cpu.regs.HL, g_cpu.regs.DE);
		/* ADD HL, HL */
		case 0x29: add_16bit(&g_cpu.regs.HL, g_cpu.regs.HL);
		/* ADD HL, SP */
		case 0x39: add_16bit(&g_cpu.regs.HL, g_cpu.regs.SP);


		/** Add n to SP **/
		/* ADD SP, i8 */
		case 0xE8: add_sp(read_address(g_cpu.regs.PC++)); break;

		/** Put SP + n effective address into HL **/
		/* LDHL SP, u8 */
		case 0xF8: 
			   uint16_t value = g_cpu.regs.SP + read_address(g_cpu.regs.PC++);
			   add_16bit(&g_cpu.regs.HL, value); break;


		/******** 8-bit RSB ********/
		/** Rotate A left. Old bit 7 to Carry Flag **/
		/* RLCA */
		case 0x07: rlca_8bit(&g_cpu.regs.AF) break;
		/** Rotate A right. Old bit 0 to Carry Flag **/
		/* RRCA */
		case 0x0F: rrca_8bit(&g_cpu.regs.AF); break;
		/** Rotate A left through Carry flag **/
		/* RLA */
		case 0x17: rla_8bit(&g_cpu.regs.AF); break;
		/** Rotate A right through Carry flag **/
		/* RRA */
		case 0x1F: rra_8bit(&g_cpu.regs.AF); break;
	}
}