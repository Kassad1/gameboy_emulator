#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include "rom.h"
//#include "mem.h"

/* Nintendo Logo */
static uint8_t logo[] = {
	0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B,
	0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
	0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
	0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
	0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC,
	0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E	
};


/* Rom Size Table */
static int romsize_table[] = {
	2, 4, 8, 16, 32, 64, 128, 256, 512
};

/* Ram Size Table */
static int ramsize_table[] = {
	0, 1, 1, 4, 16, 4
};


int rom_init(uint8_t *rombytes){

	/* Legal Cart */
	if(memcmp(&rombytes[0x104], logo, sizeof(logo)) != 0)
		return 0;

	g_mbc.type = rombtyes[CARTRIDGETYPE];
	g_mbc.romsize = romsize_table[rombytes[ROMSIZE]];

	//if (g_mbc.rombank > 50)
	//	g_mbc.rombank = 6;

	g_mbc.ramsize = ramsize_table[rombytes[RAMSIZE]];	
	/* Making space for game_bank */
	g_rom.game_bank = (uint8_t*)calloc(0x10000, sizof(uint8_t));
	memcpy(&g_rom.game_bank_rom, &rombytes, 0x8000);

	switch(g_mbc.type){
		case 0:
		break;
		/* MBC1 */
		case 1:
		case 2:
		case 3:
		        /* Default Value */
			g_rom.bank = 1;
			
			g_ram.bank = 0;
			g_ram.mode = 1;
			g_ram.enable = 0;
			/*             */
			/* Creating Space for RAM */
			/* RAM banks limit is 3 */
			g_ram.bytes = (uint8_t**)calloc(3, sizeof(uint8_t*));
			for (int i = 0 ; i < 3; ++i)
				g_ram.bytes[i] = (uint8_t*)calloc(0x1FFF, sizeof(uint8_t));
		break;
	}
}


int rom_load(const char* file){
	int f;
	size_t length;
	struct stat st;

	f = open(file, O_RDONLY);

	/* Error Handlers */
	if (f == -1)
	  return 0;

	if (fstat(f, &st) == -1)
	  return 0;

	g_rom.bytes = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, f, 0);

	/* File is empty */
	if (!g_rom.bytes)
	  return 0;

	return rom_init(g_rom.bytes);
}
