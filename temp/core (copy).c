#include "processor.h"
#include "mem.h"

/************** REG OPERATIONS **************/
uint8_t read_regs(uint16_t reg){
	return read_address(reg);
}

uint8_t read_regs_hi(uint16_t reg){
	return (reg & 0xFF00) >> 8;
}

uint8_t read_regs_lo(uint16_t reg){
	return reg & 0x00FF;
}

void write_regs_lo(uint16_t *reg, uint16_t value){
	*reg = (*reg & 0xFF00) | value;
}

void write_regs_hi(uint16_t *reg, uint16_t value){
	*reg = (value << 8)  | (*reg & 0x00FF);
}


/* Review Code */
void add_regs_lo(uint16_t *reg, uint8_t value){
	*reg = (*reg & 0xFF00) | ((*reg & 0x00FF) + value);
}

void add_regs_hi(uint16_t *reg, uint8_t value){
	*reg = (*reg & 0x00FF) | ((*reg & 0xFF00) + value);
}

void sub_regs_lo(uint16_t *reg, uint8_t value){
	*reg = (*reg & 0xFF00) | ((*reg & 0x00FF) - value);
}

void sub_regs_hi(uint16_t *reg, uint8_t value){
	*reg = (*reg & 0x00FF) | ((*reg & 0xFF00) - value);
}

/*           */


/*********************************************************/

/******************* ALU FLAGS HANDLERS ******************/
void flags_inc_dec(uint8_t prev_value, uint8_t curr_value, int state){
	g_cpu.regs.flag_z = (curr_value == 0) ? 1 : 0;
	g_cpu.regs.flag_n = (!state) ? 0 : 1;

	g_cpu.regs.flag_h = (prev_value & 0x0F) == 0x0F ? 1 : 0;
}

void flags_add(uint8_t prev_value, uint8_t curr_value){
        g_cpu.regs.flag_z = curr_value ? 1 : 0; 
	g_cpu.regs.flag_n = 0;


	uint8_t value = curr_value - prev_value;
	uint16_t half_c_check = (prev_value & 0x0F) + (value & 0x0F);

	g_cpu.regs.flag_h = half_c_check > 0x0F ? 1 : 0;

	g_cpu.regs.flag_c = (prev_value + value) > 0xFF ? 1 : 0;

}

void flags_sub(uint8_t prev_value, uint8_t curr_value){
	if(curr_value == 0)
		g_cpu.regs.flag_z = 1;

	g_cpu.regs.flag_n = 1;


	uint8_t value = curr_value - prev_value;
	uint16_t half_c_check = (prev_value & 0x0F) - (value & 0x0F);

	if(half_c_check < 0)
		g_cpu.regs.flag_h = 1;


	if((prev_value - value) < 0)
		g_cpu.regs.flag_c = 1;

}


void flags_and(void){

	g_cpu.regs.flag_z = (g_cpu.regs.AF.h == 0) ? 1 : 0;


	g_cpu.regs.flag_n = 0;

	g_cpu.regs.flag_h = 1;

	g_cpu.regs.flag_c = 0;
}


void flags_or(void){
	g_cpu.regs.flag_z = (g_cpu.regs.AF.h == 0) ? 1 : 0;

	g_cpu.regs.flag_n = 0;

	g_cpu.regs.flag_h = 0;

	g_cpu.regs.flag_c = 0;
}

void flags_xor(void){

	g_cpu.regs.flag_z = (g_cpu.regs.AF.h == 0) ? 1 : 0;

	g_cpu.regs.flag_n = 0;

	g_cpu.regs.flag_h = 0;

	g_cpu.regs.flag_c = 0;
}
/*********************************************************/


/******************** RR FLAGS HANDLERS ******************/

void flags_rotate(uint8_t value){
	g_cpu.regs.flag_n = 0;

	g_cpu.regs.flag_h = 0;

	g_cpu.regs.flag_z = (value == 0x00) ? 1 : 0;
}

/*********************************************************/

/****************** SWAP FLAGS HANDLERS ******************/
void flags_swap(uint8_t value){
	g_cpu.regs.flag_z = (value == 0x00) ? 1 : 0;


	g_cpu.regs.flag_n = 0;

	g_cpu.regs.flag_h = 0;

	g_cpu.regs.flag_c = 0;
}
/*********************************************************/


int processor_init(void)
{
  return 0;
}

int cpu_step(void)
{
  return -1;
}
