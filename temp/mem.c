#include "mem.h"


void print_current_banks(void){
	printf("ROM Bank: %d\n", g_rom.bank);
	printf("RAM Bank: %d\n", g_ram.bank);
}

uint8_t read_address(uint16_t address) {	/* Reading from ROM bank */

  
	if (address >= 0x4000 && address <= 0x7FFF){
		uint16_t tmp_addr = address;
		tmp_addr += ((g_rom.bank - 1) * 0x4000);
		return g_rom.game_bank_rom[tmp_addr];
	}

	/* Reading from RAM bank */
	if (address >= 0xA000 && address <= 0xBFFF){
		uint16_t tmp_addr = address - 0xA000;
		return g_ram.bytes[g_ram.bank][tmp_addr];
	}

	/* Come back later to add the hardware interrupt */

	return g_rom.bytes[address];
}


void write_address(uint16_t address, int8_t data){
	/* 0x0000 - 0x1FFF RAM Enable (W)*/
	if (address <= 0x1FFF){
		if((data & 0x0F) == 0x0A)
			g_ram.enable = true;
		else if (data == 0x00)
			g_ram.enable = false;
	}

	/* 0x2000 - 0x3FFF ROM Bank Number (W)*/
	if (address >= 0x2000 && address <= 0x3FFF){
		if (data == 0x00)
			data++;
		
		data &= 0x1F; /* Resetting upper bits */

		g_rom.bank &= 0xE0; /* Setting upper bits to 1 */

		g_rom.bank |= data;

		print_current_banks();
	}

	/* 0x4000 - 0x5FFF RAM Bank Number or  Upper Bits of ROM Bank number (W)*/
	if (address >= 0x4000 && address <= 0x5FFF){
		if (g_mbc.mode){
			g_ram.bank = 0;

			data &= 0x03; /* 2 bits */

			data <<= 5; /* Shifting to the right by 5 */

			if ((g_rom.bank & 0x1F) == 0)
				data++;

			g_rom.bank &= 0x1F;

			g_rom.bank |= data;

		}else
			g_ram.bank = data & 0x03;

		print_current_banks();
	}

	/* 0x6000 - 0x7FFF ROM/RAM Mode Select (W)*/
	;	if (address >= 0x6000 && address <= 0x7FFF){
		if((data & 0x01) == 0x01){
			g_mbc.mode = false;
			g_ram.bank = 0;
		}else
			g_mbc.mode = true;
	}


	/* 0xA000 - 0xBFFF External RAM */
	if (address >= 0xA000 && address <= 0xBFFF){
		if(g_mbc.ram_bank_enable){
			uint16_t temp_address = 0xA000 - address;
			g_ram.bytes[g_ram.bank][temp_address] = data;
		}

		printf("External not enabled\n");
	}

	/* 0xC000 - 0xDFFF Internal RAM */
	if (address >= 0xC000 && address <= 0xDFFF){
		g_rom.bytes[0][address] = data;
	}

	/* 0xE000 - 0xFDFF Eco Ram */
	if ((address >= 0xE000) && (address <= 0xFDFF) ){
	  g_rom.bytes[0][address] = data;
	  g_rom.bytes[0][address - 0x2000] = data;
	}
	/* 0xFE00 - 0xFE9F Object Attributed Memory (OAM) */

	/* 0xFEA0 - 0xFEFF Unused */

	/* 0xFF00 - 0xFF7F Hardware I/O Registers */

	/* 0xFFFF Interrupt Enable Register */


	/* Stack */

}
